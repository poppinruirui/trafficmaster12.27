﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScienceLeaf : MonoBehaviour {

    public ScienceTree.eBranchType m_eType = ScienceTree.eBranchType.branch0;
    public int m_nIndexOfThisBranch = 0;

    public int m_nLevel =0;

    public ScienceTreeConfig m_Config = null;

    public GameObject _goLock;
    public Image _imgOutline;
    public Image _imgMain;

    int[] m_aryIntParams = new int[8];
    float[] m_aryFloatParams = new float[8];
    bool m_bSwitch = false;



    private void Awake()
    {
       
    }

    // Use this for initialization
    void Start () {


    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickMe()
    {
        ScienceTree.s_Instance.ProcessLeaf( this );
    }

    public void SetIndex( int nIndex )
    {
        m_nIndexOfThisBranch = nIndex;
    }

    public int GetIndex()
    {
        return m_nIndexOfThisBranch;
    }


    public float GetCurValue()
    {
        if ( m_Config == null )
        {
            return 0;
        }
        return m_Config.GetValue(m_nLevel);
    }

    public float GetNextValue()
    {
        return m_Config.GetValue(m_nLevel + 1);
    }

    public int GetNextPrice()
    {
        return m_Config.GetPrice(m_nLevel + 1);
    }

    public void SetLevel( int nLevel )
    {
        m_nLevel = nLevel;

        if ( m_nLevel > 0 ) // 已解锁
        {
            //_goLock.SetActive(false);
            _imgMain.color = Color.white;
        }
        else // 未解锁
        {
            // _goLock.SetActive(true);
            _imgMain.color = ScienceTree.s_Instance.m_colorLeafLocked;
        }
    }

    public int GetLevel()
    {
        return m_nLevel;
    }

    public bool IsUnlocked()
    {
        return m_nLevel > 0;
    }

    public string GetDesc()
    {
        return m_Config.szDesc;
    }

    public ScienceTree.eScienceType GetScienceType()
    {
        return m_Config.m_eScienceType;
    }

    public void SetType(ScienceTree.eBranchType eType)
    {
        m_eType = eType;
    }

    public ScienceTree.eBranchType GetType()
    {
        return m_eType;
    }

    public void SetIntParam( int nIndex, int nValue )
    {
        m_aryIntParams[nIndex] = nValue;
    }

    public int GetIntParam(int nIndex)
    {
        return m_aryIntParams[nIndex];
    }

    public void SetFloatParam( int nIndex, float nValue)
    {
        m_aryFloatParams[nIndex] = nValue;
    }

    public float GetFloatParam(int nIndex)
    {
        return m_aryFloatParams[nIndex];
    }

    public void SetSwitch( bool bValue )
    {
        m_bSwitch = bValue;
    }

    public bool GetSwitch()
    {
        return m_bSwitch;
    }

    public void SetCanUnlock( bool bCan )
    {
        if (bCan)
        {
            if (_imgOutline)
            {
                _imgOutline.color = ScienceTree.s_Instance.m_colorCurCanUnlock;
                _imgOutline.gameObject.SetActive(true);
            }
        }
        else
        {
            if (_imgOutline)
            {
                _imgOutline.gameObject.SetActive(false);
            }
        }
    }

} // end class
