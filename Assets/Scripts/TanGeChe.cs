﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TanGeChe : MonoBehaviour {

    public static Vector3 vecTempScale = new Vector3();
    public static Vector3 vecTempPos = new Vector3();
    static Color colorTemp = new Color();

    public enum eVehicleCoounterStatus
    {
        unlocked, // 已解锁
        track_level_not_achieve_this_automobile_level, // 赛道等级尚未达到该载具等级
        track_level_achieve_this_automobile_level,// 赛道等级已达到该载具等级
    };

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtRecommendPrice;
    public Image _imgRecommendAvatar;
    public Image _imgRecommendCoinIcon;

    public GameObject _panelRecommendDetail;
    public Text _txtRecommendDetail;
    public Toggle _toggleNoPlane;
    public Toggle _toggleNoDrop;
    public InputField _inputTrackLevel;
    //// end UI

    public static TanGeChe s_Instance = null;

    public GameObject _goCounterList;
    List<UIVehicleCounter> m_lstVehicleCounters = new List<UIVehicleCounter>();
    Dictionary<string, List<UIVehicleCounter>> m_dicVehicleCounters = new Dictionary<string, List<UIVehicleCounter>>();

    public GameObject _panelTanGeChe;

    float m_fRealTimeDiscount = 0;


    public Sprite m_sprJianYingTemp;
    public Sprite m_sprLockedBuyButton;
    public Sprite m_spUnlockedBuyButton;

    public Color m_colorUnlocked;
    public Color m_colorLocked;

    public int m_nRecommendLowerAmount = 3;
    public int m_nRecommendHigherAmount = 5;

    public Image _imgPriceOff;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {


        
      
    }


	
	// Update is called once per frame
	void Update () {
        RandomEventLoop();

    }

    List<UIVehicleCounter> lstTempShowingCounters = new List<UIVehicleCounter>();
    List<UIVehicleCounter> lstTempShowingUnlockedCounters = new List<UIVehicleCounter>();

    public bool  CheckIfHigherThanOneOfTheEnergyLock( int nLevel, ref bool bEqualToEnergyLock)
    {
        bEqualToEnergyLock = false;

        Dictionary<int, ResearchManager.sResearchConfig> dicRzesearchConfig = ResearchManager.s_Instance.GetResearchConfig(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());

        // 遍历能源锁的配置
        foreach( KeyValuePair<int, ResearchManager.sResearchConfig> pair in dicRzesearchConfig)
        {
            // 判断这一级能源锁是否已经解锁(重要)
            ResearchCounter research_counter = MapManager.s_Instance.GetCurDistrict().GetResearchCounter(pair.Key);
            UIResearchCounterContainer research_container = null;
            m_dicShareUICountersContainer.TryGetValue( pair.Key, out research_container );

            if (research_counter.GetStatus() == ResearchManager.eResearchCounterStatus.unlocked){ // 这一级能源锁已经解锁
                if (research_container)
                {
                    research_container.gameObject.SetActive(false);
                }
                continue;
            }
            else{ // 这一级能源锁尚未解锁
                if (research_container)
                {
                    research_container.gameObject.SetActive(true);
                }
            }

            if ( nLevel == pair.Key )
            {
                bEqualToEnergyLock = true;
                return true;
            }
            else if (nLevel > pair.Key)
            {
                return true;
            }

        } // end foreach

        return false;
    }

    public bool CheckIfPrevEnergyNotUnlock()
    {

        return false;
    }

    public bool CheckIfEqualToResearchLevel( int nLevel )
    {
        Dictionary<int, ResearchManager.sResearchConfig> dicRzesearchConfig = ResearchManager.s_Instance.GetResearchConfig(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
        ResearchManager.sResearchConfig tempConfig;
        return dicRzesearchConfig.TryGetValue( nLevel, out tempConfig);
    }

    public float GetPriceReducePercent()
    {
        float fAdminReduceCoinPricePercent = AdministratorManager.s_Instance.GetAutomobileCoinPriceReducePercent();
        return fAdminReduceCoinPricePercent;
    }

    Dictionary<int, UIResearchCounterContainer> m_dicShareUICountersContainer = new Dictionary<int, UIResearchCounterContainer>();
    public void LoadData( int nPlanetId, int nDistrictId )
    {
        lstTempShowingCounters.Clear();
        lstTempShowingUnlockedCounters.Clear();

        foreach( KeyValuePair<int, UIResearchCounterContainer> pair in m_dicShareUICountersContainer )
        {
            pair.Value.RecycleBoundResearchCounter();
           // pair.Value.gameObject.SetActive( true );
            pair.Value.SetContainerVisible( true );
        }

        Dictionary<string, DataManager.sAutomobileConfig> dicAutomobileConfig = DataManager.s_Instance.GetAutomobileConfig();

        int i = 0;
        //for (int i = 0; i < ResourceManager.MAX_VEHICLE_NUM; i++ )
        foreach( KeyValuePair<string, DataManager.sAutomobileConfig> pair in dicAutomobileConfig  )
        {
            // 判断该交通工具是否适用于当前赛。如果不适用就跳过，不展示在工厂界面
            string szCurTrackId = MapManager.s_Instance.GetCurPlanet().GetId() + "_" + MapManager.s_Instance.GetCurDistrict().GetId();
            if ( pair.Value.szSuitableTrackId != szCurTrackId )
            {
                continue;
            }

            UIVehicleCounter counter = null;

           // m_lstVehicleCounters = null;
           // string szListKey = nPlanetId + "_" + nDistrictId;

            // 条目是复用的，不会频繁的生成和销毁
            if (i < m_lstVehicleCounters.Count)
            {
                counter = m_lstVehicleCounters[i];
            }
            else
            {
                counter = ResourceManager.s_Instance.NewVehicleCounter();
                m_lstVehicleCounters.Add(counter);
                counter.transform.SetParent(_goCounterList.transform);
                vecTempScale.x = 1f;
                vecTempScale.y = 1f;
                vecTempScale.z = 1f;
                counter.transform.localScale = vecTempScale;

                //////初始化能源锁
                int nCurLevel = i + 1;
               
              

                if (CheckIfEqualToResearchLevel(nCurLevel))
                {

                    GameObject rcc = ResearchManager.s_Instance.NewResearchCounterContainer();

                    rcc.transform.SetParent(_goCounterList.transform);

                        vecTempScale.x = 1f;
                        vecTempScale.y = 1f;
                        vecTempScale.z = 1f;
                    rcc.transform.localScale = vecTempScale;
                   // rcc.gameObject.SetActive(false);
                    m_dicShareUICountersContainer[nCurLevel] = rcc.GetComponent<UIResearchCounterContainer>();


                }

                /////

            }




            counter.gameObject.SetActive( true );
            lstTempShowingCounters.Add( counter );

            int nLevel = pair.Value.nLevel; // 本交通工具的等级和“可解锁等级”之间并没有必然联系
            int nEnergyLevel = nLevel - 1;

            eVehicleCoounterStatus eStatus = eVehicleCoounterStatus.track_level_not_achieve_this_automobile_level;


          

            // 赛道等级已经达到该载具“可解锁的等级”
            // 注意，这个“可解锁”的状态只是理论状态，还要看该载具是否受“能源研究机制”的限制
            if (MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel) 
            {

                // 载具等级小于所有“能源锁”的等级。注意，要遍历所有能源锁，而不只是某一个。
                // 比如：6级出现能源锁，而玩家故意不去解锁，一直在主场景上合成，导致载具等级一直上升，超过了6级，甚至达到11级，出现第二个能源锁。
                // 是允许同时出现两个或多个能源锁的。
                bool bEqualToEnergyLock = false;
                if ( CheckIfHigherThanOneOfTheEnergyLock(nEnergyLevel, ref bEqualToEnergyLock) ) // 载具等级大于其中任何一个能源锁的等级
                {
                    counter.SetUnlocked(false);//黑掉，不能购买

                    if (bEqualToEnergyLock) // 载具等级恰好等于（策划配置的）能源锁应该出现的等级
                    {
                       
                        // 规则修改 能源锁的状态直接跟赛道当前等级相关，不受载具的状态影响了



                        if ( CheckIfPrevEnergyNotUnlock() ) // 本来打算出现能源锁，但是前面的能源锁还没解开，所以本级能源锁不出现。
                        {

                        }
                        else // 正式出现能源锁
                        {
                            ResearchCounter rc = MapManager.s_Instance.GetCurDistrict().GetResearchCounter(nEnergyLevel);
                            UIResearchCounterContainer rc_container = m_dicShareUICountersContainer[nEnergyLevel];
                            rc_container.gameObject.SetActive( true );
                            rc.BindContainer(rc_container);

                            rc.SetActive( true );
                            counter.SetBoundResearchCounter(rc); 
                        }


                    }
                    else
                    {
                        // 只是黑掉，并不出现能源锁
                    }
                }
                else // 载具等级小于所有能源锁的等级
                {
                    counter.SetUnlocked(true);// 解锁了，可以正常购买
                    eStatus = eVehicleCoounterStatus.unlocked;
                }


            }
            else// 赛道等级尚未达到该载具“可解锁的等级”
            {
                counter.SetUnlocked(false); // 该载具黑掉、不可购买
               
               


            } // end  if (MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel)
             


            /*
              // 判断这个交通工具已解锁没有（当前赛道等级大于等于该交通工具解锁所需的等级，则自动解锁）
              if ( MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel )
              {
                  counter.SetUnlocked(true);
                  lstTempShowingUnlockedCounters.Add(counter);

                  // 还要判断该等级在“能源研究系统”中解锁没有
                  ResearchCounter rc = MapManager.s_Instance.GetCurDistrict().GetResearchCounter();
                  if (rc != null && rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked )
                  {
                      if (nLevel > rc.GetLevel())
                      {
                          counter.SetUnlocked(false);
                      }
                      else if (nLevel == rc.GetLevel())
                      {
                          //rc.transform.SetParent(counter._containerResearchLock.transform);
                          counter.SetBoundResearchCounter(rc);
                          if (rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
                          {
                              rc.gameObject.SetActive(true);
                          }                       

                          vecTempScale.x = 1f;
                          vecTempScale.y = 1f;
                          vecTempScale.z = 1f;
                          rc.transform.localScale = vecTempScale;

                          vecTempPos.x = 0;
                          vecTempPos.y = 0;
                          vecTempPos.z = 0;
                          rc.transform.localPosition = vecTempPos;
                      }

                  }
              }
              else
              {
                  counter.SetUnlocked( false );
              }
              */

            int nBuyTimes = MapManager.s_Instance.GetCurDistrict().GetVehicleBuyTimeById( pair.Value.nLevel );
            double nInitialPrice = pair.Value.nStartPrice_CoinValue;//DataManager.s_Instance.GetVehiclePrice(nPlanetId, nDistrictId, nLevel);
            for (int j = 0; j < nBuyTimes; j++ )
            {
                nInitialPrice *= ( 1f + pair.Value.fPriceRaisePercentPerBuy );
            }
            double nRealPrice = nInitialPrice;

            // 主管的技能加成：可以降低售价
            //float fAdminReduceCoinPricePercent = AdministratorManager.s_Instance.GetAutomobileCoinPriceReducePercent();
            //nRealPrice *= (1f - fAdminReduceCoinPricePercent);
            float fPriceReducePercent = GetPriceReducePercent();
            nRealPrice *= (1f - fPriceReducePercent);



            double nGain = pair.Value.nBaseGain ; // DataManager.s_Instance.GetCoinGainperRound(nPlanetId, nDistrictId, nLevel);
            float fVehiclerunTimePerRound = pair.Value.fBaseSpeed ;// DataManager.s_Instance.GetVehicleRunTimePerRound(nLevel );

            counter._txtName.text = pair.Value.szName;
            counter._txtInitialPrice.text = "原价：" + nInitialPrice.ToString();
            counter._txtSkillDiscount.text = "技能折扣：" + (0.1f * 100 ) + "%";
            //   counter._txtScienceDiscount.text = "科技树折扣：" + (fScienceDiscount * 100) + "%"; ;

            // 如果这个载具框绑定有能源锁，则把能源锁插入在它前面
            bool bShit = false;
            ResearchCounter research_counter = counter.GetBoundResearchCounter();
            if (research_counter && research_counter.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked )
            {
                /*
                research_counter.transform.SetParent(_goCounterList.transform);

                vecTempScale.x = 1f;
                vecTempScale.y = 1f;
                vecTempScale.z = 1f;
                research_counter.transform.localScale = vecTempScale;

                bShit = true;;
                */
            }
         

            counter._txtLevel.text = "lv." + nLevel;

            int nSprIndex = nLevel - 1;
            if (nSprIndex >= ResourceManager.s_Instance.m_aryParkingPlaneSprites.Length)
            {
                Debug.LogError("nSprIndex >= ResourceManager.s_Instance.m_aryParkingPlaneSprites.Length");
                nSprIndex = 0;
            }

            counter._imgAvatar.sprite = ResourceManager.s_Instance.m_aryParkingPlaneSprites[nSprIndex];

            counter._txtPrice.text = CyberTreeMath.GetFormatMoney(nRealPrice) ;//nRealPrice.ToString("f0");

            if ( !counter.GetUnlocked() )
            {
                counter._txtPrice.text = pair.Value.nCanUnlockLevel.ToString();
            }

            // 根据金币种类不同，显示的金币图标不同
            counter._imgCoinType.sprite = ResourceManager.s_Instance.m_aryCoinIcon[(int)pair.Value.eCoinType];



            counter._txtGain.text = CyberTreeMath.GetFormatMoney( nGain ) ;// nGain.ToString();
            counter._txtSpeed.text = fVehiclerunTimePerRound.ToString();

            counter.m_nPrice = nRealPrice;
            counter.m_nDiamondPrice = pair.Value.nPriceDiamond;
            counter.m_nVehicleLevel = nLevel;

            counter._txtUnLockLevel.text = pair.Value.nCanUnlockLevel.ToString();

            counter.m_Config = pair.Value;

            counter.m_bWatchAdFree = false;

            counter.m_eMoneyType = DataManager.eMoneyType.planet_0_coin;


            if (eStatus == eVehicleCoounterStatus.unlocked)
            {

            }
            else
            {
                if (MapManager.s_Instance.GetCurDistrict().GetLevel() >= nLevel)
                {
                    eStatus = eVehicleCoounterStatus.track_level_achieve_this_automobile_level;
                }
                else
                {
                    eStatus = eVehicleCoounterStatus.track_level_not_achieve_this_automobile_level;
                }
            }
            switch ( eStatus )
            {
                case eVehicleCoounterStatus.unlocked:
                    {
                        counter._containerUnlockLevelAvatar.SetActive(false);
                        counter._txtQuestionMark1.gameObject.SetActive( false );
                        counter._imgAvatar.color = Color.white;

                        counter._containerPrice.SetActive( true );
                        counter._containerUnlockLevelAvatar.SetActive( false );
                        counter._txtName.color = Color.white;
                    }
                    break;
                case eVehicleCoounterStatus.track_level_not_achieve_this_automobile_level:
                    {
                        counter._containerUnlockLevelAvatar.SetActive( true );
                        counter._txtQuestionMark1.gameObject.SetActive(true);
                        counter._imgAvatar.color = m_colorLocked;

                        counter._containerPrice.SetActive(false);
                        counter._containerUnlockLevelAvatar.SetActive(true);

                        counter._txtName.color = Color.white;
                        counter._txtName.text = "？？？？？？";
                        counter._imgUnlockLevelAvatar.gameObject.SetActive(true);
                        counter._txtQuestionMark0.gameObject.SetActive(true);


                        counter._imgUnlockLevelAvatar.sprite = ResourceManager.s_Instance.m_aryParkingPlaneSprites[pair.Value.nCanUnlockLevel - 1];
                    }
                    break;
                case eVehicleCoounterStatus.track_level_achieve_this_automobile_level:
                    {
                        counter._containerUnlockLevelAvatar.SetActive(true);
                        counter._txtQuestionMark1.gameObject.SetActive(false);
                        counter._imgAvatar.color = Color.white;

                        counter._containerPrice.SetActive(true);
                        counter._containerUnlockLevelAvatar.SetActive(true);

                        counter._txtName.color = Color.white;

                        counter._imgUnlockLevelAvatar.gameObject.SetActive( false );
                        counter._txtQuestionMark0.gameObject.SetActive(false);

                        counter._txtPrice.text = pair.Value.nCanUnlockLevel.ToString();


                    }
                    break;

            } // end switch eStatus



            /* (废弃)这里流程有问题，重写
            // 还要判断该等级在“能源研究系统”中解锁没有
            ResearchCounter rc = MapManager.s_Instance.GetCurDistrict().GetResearchCounter();
            int nTheLockLevel = -1;
            if (rc != null && rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
            {
                nTheLockLevel = rc.GetLevel() + 1;
            }

           



            // 判断这个交通工具已解锁没有（当前赛道等级大于等于该交通工具解锁所需的等级，则自动解锁）
            if (MapManager.s_Instance.GetCurDistrict().GetLevel() >= pair.Value.nCanUnlockLevel) // 赛道达到了该载具的“可解锁等级”
            {
                // counter.SetUnlocked(true);
                // lstTempShowingUnlockedCounters.Add(counter);
                if (nTheLockLevel <= 0)
                {
                    counter.SetUnlocked(true);
                }
                else
                {
                    if (nLevel > nTheLockLevel) // 其等级大于能源锁等级
                    {
                        counter.SetUnlocked(false); // 不能解锁，黑掉
                    }
                    else if (nLevel < nTheLockLevel )
                    {
                        counter.SetUnlocked(true);
                    }
                    else
                    {
                        counter.SetUnlocked(false);
                        counter.SetBoundResearchCounter(rc);
                        if (rc.GetStatus() != ResearchManager.eResearchCounterStatus.unlocked)
                        {
                            rc.gameObject.SetActive(true);
                        }

                        vecTempScale.x = 1f;
                        vecTempScale.y = 1f;
                        vecTempScale.z = 1f;
                        rc.transform.localScale = vecTempScale;

                        vecTempPos.x = 0;
                        vecTempPos.y = 0;
                        vecTempPos.z = 0;
                        rc.transform.localPosition = vecTempPos;
                    }


                 

                }
            }
 
            else// 赛道还没达到该载具可解锁等级
            {
                counter.SetUnlocked(false); 
            }
  */

          


            i++;
               } // end foreach


        int nMaxCoinBuyLevelIndex = 0;

        int nCount = 0;
        for (int iter = lstTempShowingCounters.Count - 1; iter >= 0; iter-- ) // 反向遍历已解锁的所有等级
        {
            UIVehicleCounter counter = lstTempShowingCounters[iter];
            if (counter.GetUnlocked() && iter != 0)
            {
                //// 如果是已解锁的倒数第一或第二个，则显示用钻石购买 
                if (nCount == 0 || nCount == 1)
                {
                    counter._imgCoinType.sprite = ResourceManager.s_Instance.m_aryCoinIcon[(int)DataManager.eMoneyType.diamond];
                    counter._txtPrice.text = counter.m_nDiamondPrice.ToString();
                    counter.m_eMoneyType = DataManager.eMoneyType.diamond;
                }

                if ( nCount == 4 && CanWatchAds() )
                {
                    counter.m_bWatchAdFree = true;
                    counter._imgCoinType.sprite = ResourceManager.s_Instance.m_sprWatchAdIcon;
                    counter._txtPrice.text = "免费"; 
                }

                if ( nCount == 0 )
                {
                    nMaxCoinBuyLevelIndex = iter;
                }
               
                nCount++;
            }

            if ( nCount >= 5 )
            {
                break;
            }
        }

        nMaxCoinBuyLevelIndex -= 2;
        if (nMaxCoinBuyLevelIndex < 0)
        {
            nMaxCoinBuyLevelIndex = 0;
        }
        MapManager.s_Instance.GetCurDistrict().SetMaxCoinBuyLevel(nMaxCoinBuyLevelIndex + 1);


    }

    public System.DateTime m_dtLastWatchAdsTime;
    public bool CanWatchAds()
    {
        return (Main.GetSystemTime() - m_dtLastWatchAdsTime).TotalSeconds > 120f;

       
    }

    public float GetRealTimeDiscount()
    {
        return m_fRealTimeDiscount;
    }

    public void OnClick_OpenTanGeChe()
    {
        _panelTanGeChe.SetActive( true );

        //LoadData( MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
        UpdateCarMallInfo();

        AccountSystem.s_Instance.SetGeneralMoneyCountersVisible( true );
    }

    public void OnClick_CloseTanGeChe()
    {
        AccountSystem.s_Instance.SetGeneralMoneyCountersVisible(false);
        _panelTanGeChe.SetActive(false);
    }

    public void UpdateCarMallInfo()
    {

        MapManager.s_Instance.GetCurPlanet().SetCoin(MapManager.s_Instance.GetCurPlanet().GetCoin());


      LoadData(MapManager.s_Instance.GetCurPlanet().GetId(), MapManager.s_Instance.GetCurDistrict().GetId());
    }

    ////////// 以下为“随机事件”模块
    public float m_fRandomEventInterval = 20f;
    float m_fRandomEventTimeElapse = 0f;
    bool m_bRandomEventProcessing = false;
    void RandomEventLoop()
    {

        return;
        m_fRandomEventTimeElapse += Time.deltaTime;
        if (m_fRandomEventTimeElapse >= m_fRandomEventInterval)
        {
            ExecRandomEvent();
        }
    }

    public void ExecRandomEvent()
    {
        if (m_bRandomEventProcessing)
        {
            return;
        }

        if (lstTempShowingCounters.Count < 4)
        {
            return;
        }

        UIVehicleCounter counter = lstTempShowingCounters[lstTempShowingCounters.Count - 3];
        if ( counter.m_bWatchAdFree )
        {
            return;
        }

       
        counter.m_bWatchAdFree = true;
        counter._imgCoinType.sprite = ResourceManager.s_Instance.m_sprWatchAdIcon;
        counter._txtPrice.text = "免费";

        m_fRandomEventTimeElapse = 0f;
    }

    int s_nRecommendCount = 0;
    public District.sLevelAndCost selected_one;
    public bool NeedCalculateRecommendInfo()
    {
        return s_nRecommendCount <= 0;
    }

    public void OnClick_Recommend()
    {
        // 先选择一个可用的泊位
        Lot lot = Main.s_Instance.GetAvailableLot();
        if (lot == null)
        {
            return;
        }

        selected_one = MapManager.s_Instance.GetCurDistrict().CalculateRecommendInfo();
        s_nRecommendCount--;
          


        double dCurCoin = AccountSystem.s_Instance.GetCoin();
        if (dCurCoin < selected_one.dCurPrice)
        {
            UIMsgBox.s_Instance.ShowMsg("金币不足");
            return;
        }

        Plane plane = null;
        bool bRet = Main.s_Instance.BuyOneVehicle(selected_one.nLevel, ref plane);
        if (!bRet)
        {
            UIMsgBox.s_Instance.ShowMsg("购买失败");
            return;
        }

        dCurCoin -= selected_one.dCurPrice;
        AccountSystem.s_Instance.SetCoin(dCurCoin);

        if (plane)
        {
            plane._BaseScale.BeginScale();
        }

        if (s_nRecommendCount < 0)
        {
            s_nRecommendCount = 5;
        }
    
    }

    public double GetCurPrice( Planet planet, District track, int nLevel, ref int nCurBuyTimes )
    {
        double dPrice = 0d;

        Dictionary<string, DataManager.sAutomobileConfig> dicAutomobileConfig = DataManager.s_Instance.GetAutomobileConfig();
        string szKey = planet.GetId() + "_" + track.GetId() + "_" + nLevel;
//        Debug.Log("szKey="+ szKey);
        DataManager.sAutomobileConfig config = dicAutomobileConfig[szKey];
        dPrice = config.nStartPrice_CoinValue;


        int nCurBuyTimesOfThisLevel = track.GetVehicleBuyTimeById(nLevel);
        nCurBuyTimes = nCurBuyTimesOfThisLevel;
        for (int j = 0; j < nCurBuyTimesOfThisLevel; j++)
        {
            dPrice *= (1 + config.fPriceRaisePercentPerBuy);

        } // end for j

        return dPrice;
    }

    public bool m_bNoPlane = false;
    public bool m_bNoDrop = false;
    public void OnTogggleChanged_NoPlane()
    {
        m_bNoPlane = _toggleNoPlane.isOn;



    }

    public void OnTogggleChanged_NoDrop()
    {
        m_bNoDrop = _toggleNoDrop.isOn;



    }


    public void OnInputValueChanged_TrackLevel()
    {
        District cur_track = MapManager.s_Instance.GetCurDistrict();
        int nLevel = 1;
        if ( !int.TryParse( _inputTrackLevel.text, out nLevel ))
        {
            nLevel = 1;
        }
            
        if ( nLevel > cur_track.GetLevel())
        {
            cur_track.UpdateTrackLevel( nLevel );
        }
    }

    public void OnClick_OpenRecommendDetailPanel()
    {
        _panelRecommendDetail.SetActive( true );
    }

    public void OnClick_CloseRecommendDetailPanel()
    {
        _panelRecommendDetail.SetActive(false);
    }

    public void SetPriceOffStatus( bool bPriceOff )
    {

    }
     
    public  void SetPriceOffPoPoVisible(bool bVisible)
    {
        _imgPriceOff.gameObject.SetActive(bVisible);
    }

} // end class
