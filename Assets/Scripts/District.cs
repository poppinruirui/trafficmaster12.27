﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class District : MonoBehaviour {

    public Planet m_BoundPlanet = null;

    public MapManager.eDistrictStatus m_eStatus = MapManager.eDistrictStatus.unlocked;
    public int m_nId = 0;

    public int m_nUnlockPrice = 0; //  解锁该区域所需的金币数量


    string m_szData = "";
    int m_nPrestigeTimes = 0;

    int[] m_aryVehicleBuyTimes = new int[100];
    Dictionary<int, int> m_dicVehicleBuyTimes = new Dictionary<int, int>();

    int m_nAdsRaiseTime = 0;

    // 主动技能的信息(技能是绑定在每个赛道上的，每个赛道有自己的技能状态)
    Dictionary<SkillManager.eSkillType, Skill> m_dicSkill = new Dictionary<SkillManager.eSkillType, Skill>();

    SkillManager.sSkillConfig config;

    public int m_nAdsBaseTime = 60;

    List<Plane> m_lstRunningPlanes = new List<Plane>();

    int m_nLevel = 1; // 赛道等级：本赛道上已解锁的最高级交通工具的等级就是赛道等级

    /// <summary>
    /// 主管相关
    /// </summary>
    int m_nBuyAdminTimes = 0; // 主管招募次数
 
    UIAdministratorCounter m_CurUsingAdmin = null;
    List<Admin> m_lstAdmins = new List<Admin>();
    // end 主管相关

    /// <summary>
    /// 能源研究院相关
    /// </summary>
    ResearchCounter m_ResearchCounter = null;
    // end 能源研究院相关

    /// <summary>
    ///  掉落相关
    /// </summary>
    int m_nDropLevel = 0; // 可以掉落的载具等级
    int m_nDropInterval = 0; // 掉落的时间间隔
    /// end 掉落相关

    int m_nMaxCoinBuyLevel = 0;

    // 停机坪相关

    int m_nCurLotNum = Main.MIN_LOT_NUM;


    // Use this for initialization
    void Start () {
        for (int i = 0; i < 100; i++)
        {
            m_aryVehicleBuyTimes[i] = 0;
        }

        for (int i = 0; i < (int)SkillManager.eSkillType.total_num; i++ )
        {
            Skill skill = ResourceManager.s_Instance.NewSkill();
            skill.SetBound(m_BoundPlanet, this );
            SkillManager.eSkillType eType = (SkillManager.eSkillType)i;
            skill.SetType(eType);
            ScienceTree.s_Instance.GetSkillConfig(eType, ref config);
            skill.SetConfig(config);


            m_dicSkill[(SkillManager.eSkillType)i] = skill;


        }

   


    } // end start

    // 获取这个赛道所属的星球
    public Planet GetBoundPlanet()
    {
        return m_BoundPlanet;
    }

    public Skill GetSkill( SkillManager.eSkillType eType )
    {
        Skill skill = null;
        if ( !m_dicSkill.TryGetValue(eType, out skill) )
        {
            Debug.LogError( "bug!" );
        }
        return skill;
    }
	
	// Update is called once per frame
	void Update () {
        OfflineLoop();

       
    }

    public void SetStatus(MapManager.eDistrictStatus eStatus)
    {
        m_eStatus = eStatus;
    }

    public MapManager.eDistrictStatus GetStatus()
    {
        return m_eStatus;
    }


    public int GetId()
    {
        return m_nId;
    }

    public void SetData( string szData )
    {
        m_szData = szData;
    }

    public string GetData()
    {
        return m_szData;
    }

    public double GetUnlockPrice()
    {
        return DataManager.s_Instance.GetTrackConfigById(this.GetBoundPlanet().GetId(), this.GetId()).nUnlockPrice;

        // return m_nUnlockPrice;
    }

    public void DoUnlock()
    {
        SetStatus(MapManager.eDistrictStatus.unlocked);

        MapManager.s_Instance.NewZengShouCounter( m_BoundPlanet.GetId(), GetId() );

        DataManager.s_Instance.SaveMyData("TrackUnlock" + m_BoundPlanet.GetId() + "_" + GetId(), 1);


    
    }



    public int GetPrestigeTimes()
    {
        return m_nPrestigeTimes;
    }

    public void SetPrestigeTimes(int nValue)
    {
        m_nPrestigeTimes = nValue;

        if (this == MapManager.s_Instance.GetCurDistrict())
        {
            //Main.s_Instance._txtPrestigeTimes.text = GetPrestigeTimes().ToString();
            Main.s_Instance.SetPrestigeTimes(m_nPrestigeTimes);
        }
    }

    public void DoPrestige()
    {
       
        SetPrestigeTimes(GetPrestigeTimes() + 1);
        Main.s_Instance.ClearAll();
        MapManager.s_Instance.GetCurDistrict().ClearAllRunningPlane();

        UIMsgBox.s_Instance.ShowMsg( "重生成功");

        MapManager.s_Instance.SetZengShouCounterPrestigeTimes( m_BoundPlanet.GetId(), GetId(), GetPrestigeTimes());

        UpdateTrackLevel(1, true); // 重生之后，本赛道的等级又重置为1级

        // 正在加速的(如有)全部停止
        Main.s_Instance.StopAccelerateAll();

        // 本赛道当前所有主管清空
        ClearAllAdmins();

        // 本赛道当前能源系统中的解锁情况全部重置
        // to do
        /*
        if ( this == MapManager.s_Instance.GetCurDistrict() )
        {
            //Main.s_Instance._txtPrestigeTimes.text = GetPrestigeTimes().ToString();
            Main.s_Instance.SetPrestigeTimes(GetPrestigeTimes());
        }
        */

        // 存档
        DataManager.s_Instance.SaveMyData("Prestige" + m_BoundPlanet.GetId() + "_" + GetId(), GetPrestigeTimes().ToString());

    }


   
    public void SetVehicleBuyTimeById( int nLevel, int nTimes )
    {
        m_dicVehicleBuyTimes[nLevel] = nTimes;

        // 存档
        string szData = "";
        foreach( KeyValuePair<int, int> pair in m_dicVehicleBuyTimes)
        {
            szData += ( pair.Key + "," + pair.Value );
            szData += "_";
        }
        string szKey = "BuyTimes" + m_BoundPlanet.GetId() + "_" + GetId();
        DataManager.s_Instance.SaveMyData(szKey, szData);
    }


    public int GetVehicleBuyTimeById( int nId )
    {
        int nTimes = 0;

        if ( !m_dicVehicleBuyTimes.TryGetValue( nId, out nTimes) )
        {
            nTimes = 0;
        }

        return nTimes;
    }

    System.DateTime m_dtAdsStartTime;
    int m_nAdsLeftTime = 0;
    public void SetAdsStartTime(System.DateTime dtAdsStartTime)
    {
        m_dtAdsStartTime = dtAdsStartTime;
    }

    public System.DateTime GetAdsStartTime()
    {
       return m_dtAdsStartTime;
    }

    public void GainAdsRaise()
    {
        /*
        float fAdsTimeRaise = ScienceTree.s_Instance.GetAdsTimeRaise();

        m_nAdsRaiseTime += (int)( m_nAdsBaseTime * ( 1 + fAdsTimeRaise) );
        */
        if (m_nAdsRaiseTime <= 0)
        {
            SetAdsStartTime( Main.GetSystemTime() );
        }


        m_nAdsRaiseTime += (int)AdsManager.s_Instance.GetAdsDuration();
       
        if (MapManager.s_Instance.GetCurDistrict() == this)
        {
            Main.s_Instance.UpdateRaise();
        }

        AdsManager.s_Instance._txtAdsLeftTime.gameObject.SetActive( true );
        AdsManager.s_Instance._txtTitleTiSheng.gameObject.SetActive(false);

        SaveAdsTimeData();
    }

    public void ShowAdsTime()
    {
        AdsManager.s_Instance._txtAdsLeftTime.gameObject.SetActive(true);
        AdsManager.s_Instance._txtTitleTiSheng.gameObject.SetActive(false);


        if (MapManager.s_Instance.GetCurDistrict() == this)
        {
            Main.s_Instance.UpdateRaise();
        }
    }

    public void SetAdsRaiseTime( int nRasieTime )
    {
        m_nAdsRaiseTime = nRasieTime;
    }

    public void SaveAdsTimeData()
    {
        string szKey = "AdsTime" + m_BoundPlanet.GetId() + "_" + GetId();
        string szData = m_nAdsRaiseTime + "," + GetAdsStartTime();
        DataManager.s_Instance.SaveMyData(szKey, szData);

       
    }

    public void AdsRaiseTimeLoop()
    {
        if (m_nAdsRaiseTime <= 0)
        {
            return;
        }

        // m_nAdsRaiseTime -= 1;
        m_nAdsLeftTime = m_nAdsRaiseTime - (int)((Main.GetSystemTime() - GetAdsStartTime()).TotalSeconds);
      
        if (m_nAdsLeftTime <= 0)
        {
            if (MapManager.s_Instance.GetCurDistrict() == this)
            {
                Main.s_Instance.UpdateRaise();
            }
        }
    }

    public int GetAdsLeftTime()
    {
        return m_nAdsLeftTime;//m_nAdsRaiseTime;
    }

    public float GetAdsRaise(  ref string szAdsPromoteInfo)
    {
        if (m_nAdsLeftTime <= 0)
        {
            return 0;
        }
        else
        {
            return AdsManager.s_Instance.GetAdsRaise( ref szAdsPromoteInfo);
        }
    }

    ///////////////////// 离线收益相关 /////////////////////

    System.DateTime m_fStartOfflineTime;
    public void SetStartOfflineTime(System.DateTime fTime)
    {
        m_fStartOfflineTime = fTime;
    }

    public System.DateTime GetStartOfflineTime()
    {
        return m_fStartOfflineTime;
    }

    //  离线。注意是指这个District离线，未必是整个app离线了。
    double m_fOfflineGainPerSecondBase = 0f;
    double m_fOfflineGainPerSecondReal = 0f;
    bool m_bOffline = true;

    public bool IsOffline()
    {
        return m_bOffline;
    }

    public void OffLine()
    {


        SetStartOfflineTime( Main.GetSystemTime() );

        m_bOffline = true;
        m_fOfflineGainPerSecondBase = 0;

        /*
        // 离线的时候计算一下当前的离线产出率：暂定为当前在线产出率的10%. 
        List<Plane> lstRunnig = Main.s_Instance.GetRunningList();
        for (int i = 0; i < lstRunnig.Count; i++ )
        {
            Plane plane = lstRunnig[i];
            int nGainCointPerRound = plane.GetCoinGainPerRound(); // 每一圈的收益
            float fTimeOfOneRound = Main.s_Instance.GetOneRoundTimeByLevel(plane.GetLevel()); // 每圈需要多少秒（原始值）
            float fRoundPerSecond = 1f / fTimeOfOneRound; // 每秒多少圈
            // 结合当前的技能树加成，得出每圈实际需要多少秒。这里只考虑科技树加成，不考虑主动技能加成，因为离线状态主动技能无效。
            float fScienceSpeedRaise = ScienceTree.s_Instance.GetAccelerateRaise( m_BoundPlanet.GetId() );
            fRoundPerSecond *= ( 1f + fScienceSpeedRaise);

            float fOffLineGainOfthisPlane = fRoundPerSecond * nGainCointPerRound;
            m_fOfflineGainPerSecondBase += fOffLineGainOfthisPlane;
        } // end for

        m_fOfflineGainPerSecondReal = m_fOfflineGainPerSecondBase * Main.s_Instance.UpdateRaise(m_BoundPlanet, this, true);
        */

        /*
        CalculateDPS();
        m_fOfflineGainPerSecondReal = m_fDPS * 0.1f; // 暂定离线收益是在线收益的十分之一
        */

        CalculateOfflineDps();


      //  Debug.Log( "Offline() " + m_BoundPlanet.GetId() + "_" + GetId());

        string szContent = GetOfflineDps().ToString( "f2" ) + "_" + GetStartOfflineTime();
        DataManager.s_Instance.SaveMyData("Offline" + m_BoundPlanet.GetId() + "_" + GetId(), szContent);
    }

    public void CalculateOfflineDps()
    {
        CalculateDPS();
        m_fOfflineGainPerSecondReal = m_fDPS * 0.1f; // 暂定离线收益是在线收益的十分之一
    }

    public double GetOfflineDps()
    {
        return m_fOfflineGainPerSecondReal;
    }

    public void SetOfflineDPS( double dOfflineDPS )
    {
        m_fOfflineGainPerSecondReal = dOfflineDPS;
    }

    public void SetDPS( double dDPS )
    {
        m_fDPS = dDPS;
    }

    public double GetDPS()
    {
        return m_fDPS;
    }


    double m_fDPS = 0f;
    float m_fSpeedAccelerateRate = 0f;
    public float GetSpeedAccelerateRate()
    {
        return m_fSpeedAccelerateRate;
    }

    // 当前的在线DPS(离线的DPS跟在线DPS成线性正比，暂定为十分之一)
    public double CalculateDPS()
    {
        m_fDPS = 0f;

        List<Plane> lstRunnig = m_lstRunningPlanes; //Main.s_Instance.GetRunningList();
        for (int i = 0; i < lstRunnig.Count; i++)
        {
            Plane plane = lstRunnig[i];
            double nGainCointPerRound = plane.GetCoinGainPerRound(); // 每一圈的收益(原始值)
            float fTimeOfOneRound = Main.s_Instance.GetOneRoundTimeByLevel(plane.GetLevel()); // 每圈需要多少秒（原始值）
            float fRoundPerSecond = 1f / fTimeOfOneRound; // 每秒多少圈


            //////// 速度总加成
            m_fSpeedAccelerateRate = 0;

            // 结合当前的技能树加成，得出每圈实际需要多少秒。
            float fScienceSpeedRaise = ScienceTree.s_Instance.GetAccelerateRaise(m_BoundPlanet.GetId());
           
            if (fScienceSpeedRaise > 0)
            {
                fRoundPerSecond *= (1f + fScienceSpeedRaise);
                m_fSpeedAccelerateRate += (1f + fScienceSpeedRaise);
            }

            // 主动技能造成的速度加成
            float fInitiativeAccelerate = plane.GetInitiativeAccelerateRate();
            if (fInitiativeAccelerate > 1)
            {
                fRoundPerSecond *= fInitiativeAccelerate;
                m_fSpeedAccelerateRate += fInitiativeAccelerate;

            }

            //////// end 速度总加成

            double fDpsOfthisPlane = fRoundPerSecond * nGainCointPerRound;



            m_fDPS += fDpsOfthisPlane;

           

        } // end for

        m_fDPS *= Main.s_Instance.UpdateRaise(m_BoundPlanet, this, true); // 各种加成

        // DPS存档
        string szKey = "DPS" + m_BoundPlanet.GetId() + "_" + GetId();
        DataManager.s_Instance.SaveMyData(szKey, m_fDPS.ToString());


        return m_fDPS;
    }


    public double GetCurTotalOfflineGain()
    {
        return m_fCurTotalOfflineGain;

    }

    public void SetCurTotalOfflineGain( float fValue )
    {
         m_fCurTotalOfflineGain = fValue;

        SetStartOfflineTime(Main.GetSystemTime()); // 收取离线收益后，离线时间要刷新

    }

    //  手机和电脑不一样。手机程序失去焦点之后逻辑是完全不运行的
    public float CalculateOffLineProfit( ref float fSpanTime )
    {
        System.TimeSpan fTimeElapse = Main.GetSystemTime() -  GetStartOfflineTime();

  
        if ( Cheat.s_Instance.m_bShowDebugInfo )
        {
            Main.s_Instance._txtOfflineDetailDebug.text = "离线时刻：" + GetStartOfflineTime() + "\n";
            Main.s_Instance._txtOfflineDetailDebug.text += "经历时间：" + CyberTreeMath.FormatTime((int)fTimeElapse.TotalSeconds) + " \n";
            Main.s_Instance._txtOfflineDetailDebug.text += "离线收益速度：" + m_fOfflineGainPerSecondReal;

        }
        else
        {
            Main.s_Instance._txtOfflineDetailDebug.text = "";
        }


        float fProfit = (float)( m_fOfflineGainPerSecondReal * fTimeElapse.TotalSeconds );
        fSpanTime = (float)fTimeElapse.TotalSeconds;

        return fProfit;
    }

    public void OnLine()
    {
        m_bOffline = false;

        float fSpanTinme = 0;
        Main.s_Instance.m_fOfflineProfit = CalculateOffLineProfit( ref fSpanTinme);
        if (Main.s_Instance.m_fOfflineProfit > 0 && fSpanTinme > 10f)
        {
            Main.s_Instance._panelCollectOfflineProfit.SetActive(true);
            Main.s_Instance._txtOfflineProfitOfThisDistrict.text = "+" + CyberTreeMath.GetFormatMoney( Main.s_Instance.m_fOfflineProfit);


            OfflineManager.s_Instance.UpdateOfflineProfitInfo();
        }

    }

    // 10秒计算一次收益
    const float OFFLINE_GAIN_INTERVAL = 10f;
    float m_fTimeElapseOffline = 0f;
    double m_fCurTotalOfflineGain = 0f;
    void OfflineLoop()
    {

        // 离线总收入不清零，要领取了才清零

        if ( !m_bOffline)
        {
            return;
        }

        m_fTimeElapseOffline += Time.deltaTime;
        if (m_fTimeElapseOffline < OFFLINE_GAIN_INTERVAL)
        {
            return;
        }
        m_fTimeElapseOffline = 0; 

        double fOfflineGain = m_fOfflineGainPerSecondReal * OFFLINE_GAIN_INTERVAL;
        if (true/*fOfflineGain > 10f*/)
        {
            //Debug.Log( "district[" + GetId() + "]离线收入增加：" + fOfflineGain);
            m_fCurTotalOfflineGain += fOfflineGain;
        }
    }

    /// end 离线收益相关

    public void AddRunningPlane( Plane plane )
    {
        m_lstRunningPlanes.Add(plane);
    }

    public void RemoveRunningPlane(Plane plane)
    {
        m_lstRunningPlanes.Remove(plane);
    }

    public void ClearAllRunningPlane()
    {
        m_lstRunningPlanes.Clear();
    }


    public void SetLevel( int nLevel )
    {
        m_nLevel = nLevel;
    }

    public int GetLevel()
    {
        return m_nLevel;
    }

    // 赛道等级改变
    public bool UpdateTrackLevel( int nLevel, bool bDirectly = false )
    {
        if (!bDirectly)
        {
            if (GetLevel() >= nLevel) // 不要重复执行
            {
                return false;
            }
        }


        // 存档 
        DataManager.s_Instance.SaveMyData("TrackLevel" + m_BoundPlanet.GetId() + "_" + GetId(), nLevel.ToString());

        SetLevel(nLevel);
        MapManager.s_Instance.UpdateTrackInfo();


        CalculateDropInfo();
        /* (废弃)这个能源研究机制有问题，重写了
        // 检测是否已达到“能源研究系统”中指定的瓶颈等级
        bool bFound = false;
        ResearchManager.sResearchConfig config = ResearchManager.s_Instance.GetResearchConfig( GetBoundPlanet().GetId(), GetId(), nLevel, ref bFound);
        if (bFound)
        {
            if (m_ResearchCounter == null)
            {
                m_ResearchCounter = ResearchManager.s_Instance.NewResearchCounter();
            }
            m_ResearchCounter.SetBoundTrack( this );
            m_ResearchCounter.Activate(nLevel, config);
        }
        */
        if ( this == MapManager.s_Instance.GetCurDistrict() )
        {
            SetCurLotNum(DataManager.s_Instance.GetLotNumByTrackLevel(GetLevel()));
            Main.s_Instance.UpdateLots(this);

            DataManager.s_Instance.SaveMyData( "TrackLotNum" + this.GetBoundPlanet().GetId() + "_" + this.GetId(), GetCurLotNum()  );
        }

        return true;
    }

    public void UpdateResearchRealData()
    {
        if (m_ResearchCounter == null)
        {
            return;
        }

        m_ResearchCounter.UpdateRealData();
    }

    public void CalculateAllPlanesSpeed()
    {
        for (int i = 0; i < m_lstRunningPlanes.Count; i++ )
        {
            Plane plane = m_lstRunningPlanes[i];
            plane.CalculateRunningSpeed();
        }
    }

    public bool IsCurTrack()
    {
        return this == MapManager.s_Instance.GetCurDistrict();
    }

    //// 主管相关
    // 添加一个主管
    public Admin AddAdmin( int nId )
    {
        Admin admin = AdministratorManager.s_Instance.NewAdmin();
        admin.SetConfig(AdministratorManager.s_Instance.GetAdminConfig( nId ));
        m_lstAdmins.Add( admin );
        admin.SetBoundTrack( this );

        return admin;
    }

    public void RemoveAdmin( Admin admin )
    {
        m_lstAdmins.Remove( admin );
    }


 

    // 清空所有主管
    public void ClearAllAdmins()
    {
        for (int i = m_lstAdmins.Count - 1; i >= 0; i-- )
        {
            Admin admin = m_lstAdmins[i];
            AdministratorManager.s_Instance.DeleteAdmin(admin);
        }
        m_lstAdmins.Clear();

        AdministratorManager.s_Instance.UpdateAdminMainPanelInfo();

    }

    // 获取本赛道当前已经招募的主管数。相同id的主管可以重复。重复要占用总个数
    public int GetCurBoughtAdminNum()
    {
        return m_lstAdmins.Count;
    }




    public void SetCurUsingAdmin( UIAdministratorCounter admin )
    {
        m_CurUsingAdmin = admin;
    }

    public List<Admin> GetAdminList()
    {
        return m_lstAdmins;
    }

    /// end 主管相关 
    /*
    public ResearchCounter GetResearchCounter()
    {
        return m_ResearchCounter;
    }
    */
    // 获取掉落的时间间隔
    public int GetDropInterval()
    {
       // CalculateDropInfo();

        return m_nDropInterval;
    }

    // 获取可以掉落的载具等级
    public int GetDropLevel()
    {

      //  CalculateDropInfo();

        return m_nDropLevel;
    }

    public void CalculateDropInfo()
    {
         

        string szKey = GetBoundPlanet().GetId() + "_" + GetId() + "_" + GetLevel();
        DataManager.sAutomobileConfig auto_config = DataManager.s_Instance.GetAutomobileConfig(GetBoundPlanet().GetId(), GetId(), GetLevel());
        m_nDropLevel = auto_config.nDropLevel;
        m_nDropInterval = auto_config.nDropInterval;

      //  Debug.Log( "cur drop Level ="+ m_nDropLevel);

        CalculateRecommendInfo();
    }

    // right here

    public struct sLevelAndCost
    {
        public int nLevel;
        public double dCurPrice;
        public double dCost;
        public int nCurBuyTimes;
        public int nToBuyTime;

    };
    List<sLevelAndCost> m_lstLevelAndCost = new List<sLevelAndCost>();



    public sLevelAndCost CalculateRecommendInfo( )
    {
        sLevelAndCost node_temp;
        if ( this != MapManager.s_Instance.GetCurDistrict() )
        {
            node_temp.dCost = 0;
            node_temp.dCurPrice = 0;
            node_temp.nCurBuyTimes = 0;
            node_temp.nLevel = 0;
            node_temp.nToBuyTime = 0;
            return node_temp; 
        }

        TanGeChe.s_Instance.LoadData(m_BoundPlanet.GetId(), GetId());

        int nLowLevel = m_nDropLevel;//- TanGeChe.s_Instance.m_nRecommendLowerAmount;
        if (nLowLevel < 1)
        {
            nLowLevel = 1;
        }

        int nHighLevel = m_nMaxCoinBuyLevel;//m_nDropLevel + TanGeChe.s_Instance.m_nRecommendHigherAmount;

       
        if (nHighLevel > m_nMaxCoinBuyLevel)
        {
            nHighLevel = m_nMaxCoinBuyLevel;
        }

        string szContent = "";
        szContent += "掉落等级：" + m_nDropLevel + " ,最高金币购买等级:" + m_nMaxCoinBuyLevel +  " ,最高推荐等级：" + nHighLevel + "\n";




        //        Debug.Log(m_nDropLevel + "_" + nLowLevel + "_" + nHighLevel);

        Dictionary<string, DataManager.sAutomobileConfig> dicAutomobileConfig = DataManager.s_Instance.GetAutomobileConfig();

        m_lstLevelAndCost.Clear();

        for (int i = nLowLevel; i < nHighLevel; i++ )
        {
            int nLevelDelta = nHighLevel - i;
            double nToBuyTimes = Mathf.Pow( 2 , nLevelDelta );
            int nCurBuyTimesOfThisLowLevel = GetVehicleBuyTimeById(i);
            double dTotalCostToTheHighLevel = 0;

            string szKey = m_BoundPlanet.GetId() + "_" + GetId() + "_" + i;
            DataManager.sAutomobileConfig config = dicAutomobileConfig[szKey];

            double dCurPrice = config.nStartPrice_CoinValue;
            for (int j = 0; j < nCurBuyTimesOfThisLowLevel; j++)
            {
                dCurPrice *= (1 + config.fPriceRaisePercentPerBuy );

            } // end for j


            double dPriceOfThisBuyTimes = dCurPrice;
            dTotalCostToTheHighLevel += dCurPrice;
            for (int j = 0; j < nToBuyTimes - 1; j++ )
            {
                dPriceOfThisBuyTimes *= (1 + config.fPriceRaisePercentPerBuy);
                dTotalCostToTheHighLevel += dPriceOfThisBuyTimes;
            } // end for j

          //  Debug.Log( "cost of level " + i + " : " + dTotalCostToTheHighLevel);
            sLevelAndCost node;
            node.nLevel = i;
            node.dCost = dTotalCostToTheHighLevel;
            node.dCurPrice = dCurPrice;
            node.nCurBuyTimes = nCurBuyTimesOfThisLowLevel;
            node.nToBuyTime = (int)nToBuyTimes;
            InsertSortLevelAndCost(node);


        } // end for i

        // the High level
        sLevelAndCost node_high_level;
        node_high_level.nCurBuyTimes = 0;
        double dCostOfTheHighLevel = TanGeChe.s_Instance.GetCurPrice( m_BoundPlanet, this, nHighLevel, ref node_high_level.nCurBuyTimes);

        node_high_level.nLevel = nHighLevel;
        node_high_level.dCost = dCostOfTheHighLevel;
        node_high_level.dCurPrice = dCostOfTheHighLevel;
        node_high_level.nToBuyTime = 0;
        InsertSortLevelAndCost(node_high_level);

        int nSelectedIndex = 0;
        if (!TanGeChe.s_Instance.NeedCalculateRecommendInfo())
        {
            for (int i = 0; i < m_lstLevelAndCost.Count; i++)
            {
                if (m_lstLevelAndCost[i].nLevel == TanGeChe.s_Instance.selected_one.nLevel)
                {
                    nSelectedIndex = i;
                    break;
                }
            }
        }

        float fPriceReducePercent = TanGeChe.s_Instance.GetPriceReducePercent();
        double fRealPrice = m_lstLevelAndCost[nSelectedIndex].dCurPrice;
        fRealPrice *= (1f - fPriceReducePercent);

        TanGeChe.s_Instance._txtRecommendPrice.text = CyberTreeMath.GetFormatMoney (fRealPrice/*m_lstLevelAndCost[0].dCurPrice*/);//CyberTreeMath.GetFormatMoney (m_lstLevelAndCost[0].dCost);
        TanGeChe.s_Instance._imgRecommendAvatar.sprite = ResourceManager.s_Instance.GetParkingPlaneSpriteByLevel(m_lstLevelAndCost[nSelectedIndex].nLevel);

       


        for (int i = 0; i < m_lstLevelAndCost.Count; i++ )
        {
            sLevelAndCost the_node = m_lstLevelAndCost[i];
            szContent += "等级" + the_node.nLevel + "当前购买次数" + the_node.nCurBuyTimes + ",合成到" + nHighLevel + "级还需" + the_node.nToBuyTime + "架，预计花费" + CyberTreeMath.GetFormatMoney( the_node.dCost )+ "\n";
        }

        szContent += "-------------\n";
        szContent += "决定推荐等级：" + m_lstLevelAndCost[nSelectedIndex].nLevel;

        TanGeChe.s_Instance._txtRecommendDetail.text = szContent;

        return m_lstLevelAndCost[nSelectedIndex];
       
      



    } // end CalculateRecommendInfo

    void InsertSortLevelAndCost(sLevelAndCost new_node )
    {
        bool bInserted = false;
        for (int i = 0; i < m_lstLevelAndCost.Count; i++ )
        {
            sLevelAndCost cur_node = m_lstLevelAndCost[i];
            if (new_node.dCost < cur_node.dCost)
            {
                m_lstLevelAndCost.Insert( i, new_node);
                bInserted = true;
                break;
            }

        }

        if ( !bInserted)
        {
            m_lstLevelAndCost.Add(new_node);
        }

    }

    public void ClearDicEnergyResearchCounters()
    {
        m_dicEnergyResearchCounters.Clear();
    }

    Dictionary<int, ResearchCounter> m_dicEnergyResearchCounters = new Dictionary<int, ResearchCounter>();
    public ResearchCounter GetResearchCounter( int nLockLevel )
    {
        ResearchCounter counter = null;
        if ( !m_dicEnergyResearchCounters.TryGetValue(nLockLevel, out counter) || counter == null)
        {
            counter = ResearchManager.s_Instance.NewResearchCounter();
            counter.name = "rc" + m_BoundPlanet.GetId() + "_" + GetId();
            counter.SetBoundTrack(this);

            bool bFound = false;
            int nPlanetId = m_BoundPlanet.GetId();
            int nTrackId = GetId();
            ResearchManager.sResearchConfig config = ResearchManager.s_Instance.GetResearchConfig(nPlanetId, nTrackId, nLockLevel, ref bFound);
            counter.SetConfig(config);
            counter.SetLevel(nLockLevel);
            counter.Activate(nPlanetId, nTrackId);
        }
        m_dicEnergyResearchCounters[nLockLevel] = counter;
        return counter;
    }

    public void SaveEnergyResearchData()
    {
        // 遍历所有的能源锁，全量存之 
        string szKey = "EnergyResearch" + m_BoundPlanet.GetId() + "_" + GetId();
        string szData = "";
        foreach( KeyValuePair<int, ResearchCounter> pair in m_dicEnergyResearchCounters)
        {
            szData += ( pair.Key + "," + (int)pair.Value.GetStatus() + "," + pair.Value.GetStartTime() + "," + pair.Value.GetLeftTime()+ "," + pair.Value.GetAdsLeftTime() + "," + pair.Value.GetAdsStartTime());
            szData += "_";
        } // end foreach

        DataManager.s_Instance.SaveMyData(szKey, szData);
    }


    public void SetResearchCounterInfo( int nLevel, ResearchManager.eResearchCounterStatus eStatus, System.DateTime dtStartTime, int nLeftTime, int nAdsLefTime, System.DateTime dtAdsStartTime )
    {

        ResearchCounter counter = GetResearchCounter(nLevel);
        counter.SetStatus(eStatus);
        counter.SetStartTime(dtStartTime);
        counter.SetLeftTime( nLeftTime );
        counter.SetAdsLeftTime( nAdsLefTime );
        if (eStatus == ResearchManager.eResearchCounterStatus.unlocking)
        {
            counter.PreUnlockSucceed( true );
            if ( nAdsLefTime > 0 )
            {
                counter.SetAdsLeftTime(nAdsLefTime);
                counter.SetAdsStartTime( dtAdsStartTime );
                counter.BeginAdsInterval( true);
            }
        }
        else if (eStatus == ResearchManager.eResearchCounterStatus.unlocked)
        {
            counter.UnlockSucceeded(true);
        }
    }

    float m_fTrackCoinRaise = 0f; // 赛道的金币加成

    public float GetTrackRaise()
    {
        return m_fTrackCoinRaise;
    }

    public void SetTrackRaise( float fTrackCoinRaise)
    {
        m_fTrackCoinRaise = fTrackCoinRaise;
    }

    public void SetMaxCoinBuyLevel( int nLevel )
    {
        m_nMaxCoinBuyLevel = nLevel;
    }

    public int GetMaxCoinBuyLevel()
    {
        return m_nMaxCoinBuyLevel;
    }


    public int GetCurLotNum()
    {
        return m_nCurLotNum;
    }

    public void SetCurLotNum( int nCurLotNum )
    {
        m_nCurLotNum = nCurLotNum;
    }

} // end class
