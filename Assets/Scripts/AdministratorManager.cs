﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class AdministratorManager : MonoBehaviour {

  
    

    public Sprite[] m_arySkillIcons;

    public GameObject _effectCasting;

    public int MAX_QUALITY_LEVEL = 3;

    static Vector3 vecTempScale = new Vector3();
    static Vector3 vecTempPos = new Vector3();
    public BaseScale _basescaleCPosCoinIcon;

    public Vector3 m_vecCoinFlyStartPos = new Vector3();
    public Vector3 m_vecCoinFlyMiddlePos_Left = new Vector3();
    public Vector3 m_vecCoinFlyMiddlePos_Right = new Vector3();
    public Vector3 m_vecCoinFlyEndPos = new Vector3();
    public float m_fSkillGainInterval = 5;

    public static AdministratorManager s_Instance = null;

    //// Data
    public Vector3 m_vecTiaoZiPos = new Vector3();
    public Vector3 m_vecInitAndMaxScale = new Vector3();
    /// end Data


    int m_nAvatarIndex = 0;

    public Color m_colorUsing;
    public Color m_colorNotUsing;

    public Sprite[] m_aryAvatar;
    public Sprite[] m_aryAvatar_Small;


    public Sprite m_sprNotUsingBg;
    public Sprite m_sprUsingBg;

    /// <summary>
    /// prefab
    /// </summary>
    public GameObject m_preAdminCounter;
    public GameObject m_preAdmin;
    /// end prefab

    /// panel and containers
    public GameObject _panelMain;
    public Image _imgSceneSkillIcon;
    public GameObject _subpanelUsing;
    public GameObject _containerNotUsing;
 
    public GameObject _subpanelNotUsing;
    public GameObject _containerUsing;
    List<UIAdministratorCounter> m_lstAllCounters = new List<UIAdministratorCounter>();

    public GameObject m_containerRecycledCounters;

    public GameObject _subpanelSell; // “卖出”界面

    public GameObject _containerTiaoZi; //  跳字

    /// end panel and containers

    //// UI
    public Image _imgMoneyIcon; // 招募价钱金币图标  
    public Text _txtPrice;      // 招募价钱

    public Button _btnCastSkill;       // 施放主管技能
    public Image _imgUsingAdminAvatar; // 正在使用的主管角色形象
    public Image _imgAvatarMask;

    public Text _txtLeftTime;

    public Text _txtBuyPrice;       // 购入时价格
    public Image _imgSellMoneyIcon; // 卖出时金币图标

    public Text _txtNum;            // 已招募数/可招募数

    //// end UI


    int m_nUnlockTrackLevel = 0; // 可以解锁管家系统的赛道等级
    int m_nMaxBuyNum = 0;        // 最多招募个数
    float m_fPriceRisePercent = 0; // 每次购买的涨价百分比

    double m_nCurPrice = 0;

    //UIAdministratorCounter m_CurUsingAdmin = null;
    Admin m_CurUsingAdmin = null;

    Admin m_AdminToSell = null;

    public CFrameAnimationEffect _effectCastingSkill;

    public enum eAdminFuncType
    {
        automobile_accelerate,   // 载具加速
        coin_raise,              // 金币收益加成
        gain_immediately,        // 瞬间获取收益
        reduce_automobile_price, // 减少载具价格
    };

    // 正在使用的那个主管的具体状态
    public enum eUisngAdminStatus
    {
        idle,     // 空闲
        casting,  // 施放技能中
        colddown, // 技能冷却
    };

    public struct sAdminConfig
    {
        public int nId;                 // id
        public string szName;           // 名称
        public string szQuality;        // 品质
        public int nQuality; // 品质（同一类中的等级）
        public string szDesc;           // Desc
        public eAdminFuncType eType;    // 功能类型
        public float fValue;            // 功能数值
        public int nDuration;           // 持续时间
        public int nColddown;           // 冷却时间
        public int nProbabilityWeight;  // 招募概率权重
    };
    sAdminConfig tempAdminConfig;
    Dictionary<int, sAdminConfig> m_dicAdminConfig = new Dictionary<int, sAdminConfig>();

    List<int> m_lstProbabilityPool = new List<int>();

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {


        string szConfigFileName_Admin =  DataManager.url + "Admin.csv";
        StartCoroutine(LoadConfig_Admin(szConfigFileName_Admin));
    //    LoadConfigOffline_Admin(szConfigFileName_Admin);




    }

    public sAdminConfig GetAdminConfig( int nId )
    {
        if ( !m_dicAdminConfig.TryGetValue( nId, out tempAdminConfig ) )
        {
            Debug.LogError( "error" );
        }

        return tempAdminConfig;
    }

    void ParseConfig( string szAll)
    {
        string[] aryLines = szAll.Split('\n');

        string[] aryGeneralParams = aryLines[1].Split(',');

        int nColIndex = 0;

        // 整体配置项

        m_nUnlockTrackLevel = int.Parse(aryGeneralParams[nColIndex++]);
        m_nMaxBuyNum = int.Parse(aryGeneralParams[nColIndex++]);
        m_fPriceRisePercent = float.Parse(aryGeneralParams[nColIndex++]);

        // 每个管理员的配置
        for (int i = 3; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            nColIndex = 1;
            sAdminConfig config = new sAdminConfig();
            config.nId = nId;
            config.szName = aryParams[nColIndex++];
            config.szQuality = aryParams[nColIndex++];
            config.nQuality = 0;
            if (!int.TryParse(config.szQuality, out config.nQuality))
            {
                config.nQuality = 0;
            }

            config.szDesc = aryParams[nColIndex++];
            config.eType = (eAdminFuncType)int.Parse(aryParams[nColIndex++]);
            config.fValue = float.Parse(aryParams[nColIndex++]);
            config.nDuration = int.Parse(aryParams[nColIndex++]);
            config.nColddown = int.Parse(aryParams[nColIndex++]);
            config.nProbabilityWeight = int.Parse(aryParams[nColIndex++]);

            for (int j = 0; j < config.nProbabilityWeight; j++)
            {
                m_lstProbabilityPool.Add(nId);
            } // end for j

            m_dicAdminConfig[nId] = config;

        } // end for i

        m_bAdminConfigLoaded = true;
        DataManager.s_Instance.TryLoadMyDataCurTrackPlanesData();

    }

    public void LoadConfigOffline_Admin(string szFileName)
    {
        StreamReader sr = new StreamReader(szFileName);
        string szAll = sr.ReadToEnd();

        ParseConfig(szAll);
        /*         string[] aryLines = szAll.Split('\n');

        string[] aryGeneralParams = aryLines[1].Split(',');

        int nColIndex = 0;

        // 整体配置项

        m_nUnlockTrackLevel = int.Parse(aryGeneralParams[nColIndex++]);
        m_nMaxBuyNum = int.Parse(aryGeneralParams[nColIndex++]);
        m_fPriceRisePercent = float.Parse(aryGeneralParams[nColIndex++]);

        // 每个管理员的配置
        for (int i = 3; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            nColIndex = 1;
            sAdminConfig config = new sAdminConfig();
            config.nId = nId;
            config.szName = aryParams[nColIndex++];
            config.szQuality = aryParams[nColIndex++];
            config.nQuality = 0;
            if ( int.TryParse(config.szQuality, out config.nQuality) )
            {
                config.nQuality = 0;
            }

            config.szDesc = aryParams[nColIndex++];
            config.eType = (eAdminFuncType)int.Parse(aryParams[nColIndex++]);
            config.fValue = float.Parse(aryParams[nColIndex++]);
            config.nDuration = int.Parse(aryParams[nColIndex++]);
            config.nColddown = int.Parse(aryParams[nColIndex++]);
            config.nProbabilityWeight = int.Parse(aryParams[nColIndex++]);

            for (int j = 0; j < config.nProbabilityWeight; j++)
            {
                m_lstProbabilityPool.Add(nId);
            } // end for j

            m_dicAdminConfig[nId] = config;

        } // end for i

        m_bAdminConfigLoaded = true;
        DataManager.s_Instance.TryLoadMyDataCurTrackPlanesData();
        */

    } // end LoadConfigOffline_Admin

    public bool m_bAdminConfigLoaded = false;
    IEnumerator LoadConfig_Admin(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; //  待下载

        ParseConfig(www.text);
        /*
        string[] aryLines = www.text.Split('\n');
        string[] aryGeneralParams = aryLines[1].Split( ',' );

        int nColIndex = 0;

        // 整体配置项

        m_nUnlockTrackLevel = int.Parse(aryGeneralParams[nColIndex++]);
        m_nMaxBuyNum = int.Parse(aryGeneralParams[nColIndex++]);
        m_fPriceRisePercent = float.Parse(aryGeneralParams[nColIndex++]);

        // 每个管理员的配置
        for (int i = 3; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');
            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            nColIndex = 1;
            sAdminConfig config = new sAdminConfig();
            config.nId = nId;
            config.szName = aryParams[nColIndex++];
            config.szQuality = aryParams[nColIndex++];
            config.szDesc = aryParams[nColIndex++];
            config.eType = (eAdminFuncType)int.Parse(aryParams[nColIndex++]);
            config.fValue = float.Parse(aryParams[nColIndex++]);
            config.nDuration = int.Parse(aryParams[nColIndex++]);
            config.nColddown = int.Parse(aryParams[nColIndex++]);
            config.nProbabilityWeight = int.Parse(aryParams[nColIndex++]);

            for (int j = 0; j < config.nProbabilityWeight; j++ )
            {
                m_lstProbabilityPool.Add( nId );
            } // end for j

            m_dicAdminConfig[nId] = config;

        } // end for i

        m_bAdminConfigLoaded = true;
        DataManager.s_Instance.TryLoadMyDataCurTrackPlanesData();
        */

    } // end LoadConfig_Admin

    // Update is called once per frame
    void Update () {
        SkillLoop();
	}

    public void SetMainPanelVisible( bool bVisible )
    {
        _panelMain.SetActive(bVisible);


        if ( bVisible )
        {
            UpdateAdminMainPanelInfo();
        }
    }


    List<UIAdministratorCounter> m_lstRecycledCounters = new List<UIAdministratorCounter>();
    public UIAdministratorCounter NewAdminCounter()
    {
        UIAdministratorCounter counter = null;

        if (false/*m_lstRecycledCounters.Count > 0*/)
        {
            counter = m_lstRecycledCounters[0];
            counter.gameObject.SetActive( true );
            m_lstRecycledCounters.RemoveAt(0);
            counter.Reset();
        }
        else
        {
            counter = GameObject.Instantiate( m_preAdminCounter ).GetComponent<UIAdministratorCounter>();
        }

        return counter;
    }

    public void DeleteAdminCounter(UIAdministratorCounter counter)
    {
        //counter.gameObject.SetActive( false );
        //m_lstRecycledCounters.Add( counter );
        counter.transform.SetParent( m_containerRecycledCounters.transform );
        GameObject.Destroy( counter.gameObject );
    }

    List<Admin> m_lstRecycledAdmins = new List<Admin>();
    public Admin NewAdmin()
    {
        Admin admin = null;

        if (false/*m_lstRecycledAdmins.Count > 0*/ )
        {
            admin = m_lstRecycledAdmins[0];
            m_lstRecycledAdmins.RemoveAt( 0 );
            admin.gameObject.SetActive( true );
            admin.Reset();
        }
        else
        {
            admin = GameObject.Instantiate( m_preAdmin ).GetComponent<Admin>();
        }

        return admin;
    }

    public void DeleteAdmin( Admin admin )
    {
        GameObject.Destroy(admin.gameObject);
      //  admin.gameObject.SetActive(false);
      //  m_lstRecycledAdmins.Add( admin );
    }

    public void DestroyAdmin(Admin admin)
    {
        MapManager.s_Instance.GetCurDistrict().RemoveAdmin(admin);
        m_lstAllCounters.Remove(admin.GetBoundCounter());
        DeleteAdminCounter(admin.GetBoundCounter());
        DeleteAdmin( admin );
    }


    public void OnClickButton_OpenAdminMainPanel()
    {
        if ( MapManager.s_Instance.GetCurDistrict().GetLevel() < m_nUnlockTrackLevel )
        {
            UIMsgBox.s_Instance.ShowMsg( "赛道等级" + m_nUnlockTrackLevel + "才能使用主管");
            return;
        }

        SetMainPanelVisible( true );
    }

    public void OnClickButton_CloseAdminMainPanel()
    {
        SetMainPanelVisible(false);
    }

    public void OnClickButton_Buy()
    {
        District cur_track = MapManager.s_Instance.GetCurDistrict();
        Planet planet = MapManager.s_Instance.GetCurPlanet();
        int nPlanetId = planet.GetId();

        // 先判断已招募的个数有没有达到上限，如果达到了就不能再招募了
        int nCurBuyNum = cur_track.GetCurBoughtAdminNum();
        if (nCurBuyNum >= m_nMaxBuyNum)
        {
            UIMsgBox.s_Instance.ShowMsg( "已达到招募上限" );
            return;
        }

        // 判断金币够不够
        double nCurCoin = AccountSystem.s_Instance.GetCoin(nPlanetId);
        if (nCurCoin < m_nCurPrice)
        {
            UIMsgBox.s_Instance.ShowMsg( "金币不够" );
            return;
        }

        // 根据配置的比率，随机招募一个主管
        int nAdminId = RandomBuyOneAdmin();
        // Admin admin = cur_track.AddAdmin(nAdminId);
        //admin.SetBuyPrice(m_nCurPrice);
        DoBuy(cur_track, nAdminId, m_nCurPrice);


        // 消耗金币

        nCurCoin -= m_nCurPrice;
        AccountSystem.s_Instance.SetCoin(nPlanetId, nCurCoin);

        // 购买成功，刷新主管系统主界面内容
        UpdateAdminMainPanelInfo();

        UIMsgBox.s_Instance.ShowMsg( "招募成功" );


       

        // 存档
        SaveAdminData();


    }

    public Admin DoBuy( District cur_track, int nAdminId, double nCurPrice )
    {
        Admin admin = cur_track.AddAdmin(nAdminId);
        admin.SetBuyPrice(m_nCurPrice);

     //   UpdateAdminMainPanelInfo();

        return admin;
    }

    public void SaveAdminData()
    {
        Planet cur_planet = MapManager.s_Instance.GetCurPlanet();
        District cur_track = MapManager.s_Instance.GetCurDistrict();

        string szKey = "Admin" + cur_planet.GetId() + "_" + cur_track.GetId();
        string szData = "";

        List<Admin> lstAdmins = cur_track.GetAdminList();
        for (int i = 0; i < lstAdmins.Count; i++)
        {

            Admin admin = lstAdmins[i];

            int nLeftTime = 0;
            System.DateTime dtStartTime = System.DateTime.MaxValue;
            if ( admin.GetStatus() == eUisngAdminStatus.casting )
            {
                nLeftTime = admin.GetSkillCastingLeftTime();
                dtStartTime = admin.GetStartTime();
            }
            else if (admin.GetStatus() == eUisngAdminStatus.colddown)
            {
                nLeftTime = admin.GetSkillColdDownLeftTime();
                dtStartTime = admin.GetStartTime();
            }


            szData += ( admin.GetConfig().nId + "," +
                            (admin.GetUsing()?1: 0) + "," +
                            (int)admin.GetStatus() + "," +
                            dtStartTime

                          );
                if ( i != lstAdmins.Count - 1)
                {
                    szData += "_";
                }


        } // end for i

        DataManager.s_Instance.SaveMyData(szKey, szData);


    }  // end SaveAdminData()

    public int RandomBuyOneAdmin()
    {
        int nIndex = CyberTreeMath.GetRandomIndex( 0, m_lstProbabilityPool.Count - 1);
        int nAdminId = m_lstProbabilityPool[nIndex];
        return nAdminId;
    }

    public Sprite GetAvatarByTypeAndQuality( int nType, int nQuality, bool bSmall = false )
    {
        Sprite spr = null;

        int nIndex = nType * MAX_QUALITY_LEVEL + nQuality;
        if ( bSmall )
        {
            return m_aryAvatar_Small[nIndex];
        }
        else
        {
            return m_aryAvatar[nIndex];
        }

        return spr;
    }

    // 刷新管家系统主界面信息
    // 什么时候需要刷新界面？ 打开界面、购买、指派、取消指派、售出
    public void UpdateAdminMainPanelInfo()
    {
        ////Do Some Clear
        m_CurUsingAdmin = null;
        ClearAllNotUsingCounters();

        _imgAvatarMask.gameObject.SetActive( false );
     //   _effectCastingSkill.gameObject.SetActive( false );
        _effectCasting.gameObject.SetActive(false);
        _btnCastSkill.gameObject.SetActive( false );
        _txtLeftTime.gameObject.SetActive(false);
        /// end Do Some Clear

       //// 当前招募价钱
        District cur_track = MapManager.s_Instance.GetCurDistrict();
        Planet cur_planet = MapManager.s_Instance.GetCurPlanet();
       int nPlanetId = cur_planet.GetId();
       int nTrackId = cur_track.GetId();
        double nBasePrice = DataManager.s_Instance.GetTrackConfigById(nPlanetId, nTrackId).nBuyAdminBasePrice;
        int nBuyTimes = cur_track.GetCurBoughtAdminNum();
        double fRealPrice = nBasePrice;
        for (int i = 0; i < nBuyTimes; i++)
        {
            fRealPrice *= ( 1 + m_fPriceRisePercent );
        }

        // 受技能树影响
        ScienceLeaf leaf = ScienceTree.s_Instance.GetLeaf(ScienceTree.eBranchType.branch2, 4);
        if (leaf.GetLevel() > 0)
        {
            fRealPrice *= (1f - leaf.GetFloatParam(0));
        }


        m_nCurPrice = (int)fRealPrice;

        _txtPrice.text = CyberTreeMath.GetFormatMoney( fRealPrice );
        _imgMoneyIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(nPlanetId);

        Admin using_admin = null;
        m_CurUsingAdmin = null;
        // 刷新“已招募”列表
       
        List<Admin> lstAdmins = cur_track.GetAdminList();
        for (int i = 0; i < lstAdmins.Count; i++ )
        {
           
            Admin admin = lstAdmins[i];
         
            if ( admin.GetUsing() )
            {
                m_CurUsingAdmin = admin;
                using_admin = admin;
                continue;
            }
            UIAdministratorCounter counter = NewAdminCounter();
            AddCounterToNotUsingList(counter);
            counter.SetUsing( false);
            counter.Bind(admin);

            /*
            counter._imgAvatar.sprite = m_aryAvatar[m_nAvatarIndex++];
            if (m_nAvatarIndex >= m_aryAvatar.Length)
            {
                m_nAvatarIndex = 0;
            }
            */
           


        }

        //  显示当前已招募和可招募的数量
        _txtNum.text = lstAdmins.Count + "/" + m_nMaxBuyNum;


        // 刷新“正在使用”列表
        _btnCastSkill.gameObject.SetActive(false);
        _imgAvatarMask.gameObject.SetActive(false);
        _imgUsingAdminAvatar.gameObject.SetActive(false);

        if (using_admin != null)
        {
            UIAdministratorCounter counter = NewAdminCounter();
            counter.Bind(using_admin);
            counter.SetUsing(true);
            AddCounterToUsingList(counter);

            _imgUsingAdminAvatar.sprite = counter.GetSmallAvatar();
            _imgUsingAdminAvatar.gameObject.SetActive(true);

            if (using_admin.GetStatus() == eUisngAdminStatus.idle)
            {
                _btnCastSkill.gameObject.SetActive(true);
                _txtLeftTime.gameObject.SetActive(false);
            }
            else if (using_admin.GetStatus() == eUisngAdminStatus.colddown)
            {
                _txtLeftTime.gameObject.SetActive(true);
                _imgAvatarMask.gameObject.SetActive(true);
            }
            else if (using_admin.GetStatus() == eUisngAdminStatus.casting)
            {
                _txtLeftTime.gameObject.SetActive(true);
                //_effectCastingSkill.BeginPlay(true);
               // _effectCastingSkill.gameObject.SetActive( true );
                _effectCasting.SetActive(true);
            }

        }

        bool bShowPriceOff = false;
        if (m_CurUsingAdmin)
        {
            _imgSceneSkillIcon.sprite = m_arySkillIcons[(int)(m_CurUsingAdmin.GetConfig().eType)];
            if ( m_CurUsingAdmin.GetConfig().eType == eAdminFuncType.reduce_automobile_price )
            {
                bShowPriceOff = true;
            }
        }
    }

    void AddCounterToNotUsingList(UIAdministratorCounter counter)
    {
        m_lstAllCounters.Add(counter);
        counter.transform.SetParent( _containerNotUsing.transform );
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        counter.transform.localScale = vecTempScale;
    }

    void AddCounterToUsingList(UIAdministratorCounter admin)
    {
        m_lstAllCounters.Add(admin);

        admin.transform.SetParent(_containerUsing.transform);

        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        admin.transform.localScale = vecTempScale;

        vecTempPos.x = 0.5f;
        vecTempPos.y = 0.5f;
        admin.GetComponent<RectTransform>().anchorMax = vecTempPos;
        admin.GetComponent<RectTransform>().anchorMin = vecTempPos;


        vecTempPos.x = 0f;
        vecTempPos.y = 0f;
        vecTempPos.z = 0f;
        admin.transform.localPosition = vecTempPos;
    }

    void ClearAllNotUsingCounters()
    {
        for (int i = 0; i < m_lstAllCounters.Count; i++ )
        {
            UIAdministratorCounter counter = m_lstAllCounters[i];
            DeleteAdminCounter(counter);
        }
        m_lstAllCounters.Clear();
    }
    /*
    public void DoUse( UIAdministratorCounter couter_admin )
    {

        if (m_CurUsingAdmin != null)
        {
            m_CurUsingAdmin.SetUsing(false);
        }

        Admin admin = couter_admin.GetBoundAdmin();
        admin.SetUsing( true );
        m_CurUsingAdmin = admin;





        UpdateAdminMainPanelInfo();

        // 存档
        SaveAdminData();

    }
    */
    public void DoUse(Admin admin)
    {

        if (m_CurUsingAdmin != null)
        {
            m_CurUsingAdmin.SetUsing(false);
        }

      
        admin.SetUsing(true);
        m_CurUsingAdmin = admin;





     //   UpdateAdminMainPanelInfo();

      

    }



    public void DoCancel(UIAdministratorCounter admin)
    {
        if ( m_CurUsingAdmin == null )
        {
            Debug.LogError( "error" );
            return;
        }

        m_CurUsingAdmin.CancelCasting();
        m_CurUsingAdmin.SetUsing( false );

        m_CurUsingAdmin = null;

        _imgAvatarMask.gameObject.SetActive( false );

       UpdateAdminMainPanelInfo();
    }

    // 使用管家技能(类似以前的“主动技能”)
    public void OnClickButton_CastSkill()
    {
        //  _effectCastingSkill.gameObject.SetActive(true);
        //  _effectCastingSkill.BeginPlay(true);
        _effectCasting.SetActive( true );

        m_CurUsingAdmin.BeginCastSkill(  );
        _btnCastSkill.gameObject.SetActive( false );

        UpdateAdminMainPanelInfo();


        SaveAdminData();    
    }

    public void EndCastSkill()
    {


        _txtLeftTime.text = "";

        _imgAvatarMask.gameObject.SetActive( true );

        _effectCastingSkill.gameObject.SetActive(false);
        _effectCasting.SetActive(false);

        UpdateAdminMainPanelInfo();
    }

    public void BeginColdDown()
    {
        _imgAvatarMask.gameObject.SetActive(true);
        _effectCastingSkill.gameObject.SetActive(false);
        _effectCasting.SetActive(false);
    }

    public void EndColdDown()
    {

        UpdateAdminMainPanelInfo();

    }

    public float GetCoinRaise( ref float fInitPromote, ref float fScienceTreePromote  )
    {
        if ( m_CurUsingAdmin == null )
        {
            return 0;
        }

        if ( m_CurUsingAdmin.GetConfig().eType != eAdminFuncType.coin_raise  )
        {
            return 0;
        }

        if ( m_CurUsingAdmin.GetStatus() != eUisngAdminStatus.casting )
        {
            return 0;
        }

        float fRealValue = m_CurUsingAdmin.GetConfig().fValue;
        fInitPromote = m_CurUsingAdmin.GetConfig().fValue;
        // 受科技树影响
        fScienceTreePromote = 0;
        ScienceLeaf leaf = ScienceTree.s_Instance.GetLeaf(ScienceTree.eBranchType.branch2, 1);
        if (leaf.GetLevel() > 0)
        {
            fScienceTreePromote = leaf.GetFloatParam(0);
            fRealValue *= (1f + leaf.GetFloatParam(0));
        }
        //Debug.Log("fRealValue:" + m_CurUsingAdmin.GetConfig().fValue + "," + leaf.GetFloatParam(0) + "," + fRealValue);
        return fRealValue;
        //return m_CurUsingAdmin.GetConfig().fValue;
    }

    public float GetAutomobileSpeedAccelerate()
    {
        if (m_CurUsingAdmin == null)
        {
            return 0;
        }

        if (m_CurUsingAdmin.GetConfig().eType != eAdminFuncType.automobile_accelerate)
        {
            return 0;
        }

        if (m_CurUsingAdmin.GetStatus() != eUisngAdminStatus.casting)
        {
            return 0;
        }


        // return m_CurUsingAdmin.GetConfig().fValue;
        float fRealValue = m_CurUsingAdmin.GetConfig().fValue; // 配置的原始值

        // 受科技树影响
        ScienceLeaf leaf = ScienceTree.s_Instance.GetLeaf( ScienceTree.eBranchType.branch2, 0 );
        if (leaf.GetLevel() > 0)
        {
            fRealValue *= (1f + leaf.GetFloatParam(0) );
        }

    //    Debug.Log("GetAutomobileSpeedAccelerate=" + fRealValue);
        return fRealValue;
    }

    // 获取主管对载具售价的降低百分比
    public float GetAutomobileCoinPriceReducePercent()
    {
        if (m_CurUsingAdmin == null)
        {
            return 0;
        }

        if (m_CurUsingAdmin.GetConfig().eType != eAdminFuncType.reduce_automobile_price)
        {
            return 0;
        }

        if (m_CurUsingAdmin.GetStatus() != eUisngAdminStatus.casting)
        {
            return 0;
        }

        float fRealValue = m_CurUsingAdmin.GetConfig().fValue;
        // 受科技树影响
        ScienceLeaf leaf = ScienceTree.s_Instance.GetLeaf(ScienceTree.eBranchType.branch2, 3);
        if (leaf.GetLevel() > 0)
        {
            fRealValue *= (1f + leaf.GetFloatParam(0));
        }
        Debug.Log( "real value=" + m_CurUsingAdmin.GetConfig() + "," + leaf.GetFloatParam(0) + "," + fRealValue);
        return fRealValue/*m_CurUsingAdmin.GetConfig().fValue*/;
    }


    void SkillLoop()
    {
        if ( m_CurUsingAdmin == null )
        {
            return;
        }

        switch( m_CurUsingAdmin.GetStatus() )
        {
            case eUisngAdminStatus.casting:
                {
                    _txtLeftTime.text = CyberTreeMath.FormatTime( m_CurUsingAdmin.GetSkillCastingLeftTime() );
                }
                break;
            case eUisngAdminStatus.colddown:
                {
                    _txtLeftTime.text = CyberTreeMath.FormatTime( m_CurUsingAdmin.GetSkillColdDownLeftTime());
                    _imgAvatarMask.fillAmount = m_CurUsingAdmin.GetColdDownPercent();
                }
                break;
        }// end switch
    }

    public void CancelCastingSkill()
    {
        if ( m_CurUsingAdmin == null )
        {
            return;
        }

        m_CurUsingAdmin.CancelCasting();

        UpdateAdminMainPanelInfo();
    }


    public void SetSellPanelVisible( bool bVisibel )
    {
        _subpanelSell.SetActive( bVisibel );
    }

    public void OnClickButton_CloseSellPanel()
    {
        SetSellPanelVisible(false);
    }

    public void OnClickButton_WatchAdsFullPrice()
    {
        AdsManager.s_Instance.PlayAds();


        DoSell(m_AdminToSell.GetBuyPrice());
    }

    public void OnClickButton_HalfPrice()
    {
        double nSellPrice = m_AdminToSell.GetBuyPrice() / 2d;
        DoSell(nSellPrice);
    }

    public void DoSell( double nSellPrice )
    {
        AccountSystem.s_Instance.ChangeCoin(1, nSellPrice);

        DestroyAdmin(m_AdminToSell);

        UIMsgBox.s_Instance.ShowMsg("出售成功，获得金币" + CyberTreeMath.GetFormatMoney( nSellPrice ) );

        SetSellPanelVisible(false);

        UpdateAdminMainPanelInfo();

        AdministratorManager.s_Instance.SaveAdminData();
    }

    public void SellAdmin( Admin admin )
    {
        SetSellPanelVisible( true );
        m_AdminToSell = admin;

        _txtBuyPrice.text = CyberTreeMath.GetFormatMoney( admin.GetBuyPrice() );
        _imgMoneyIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId( MapManager.s_Instance.GetCurPlanet().GetId() );
    }

  


} // end class
