﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResearchCounter : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    ResearchManager.sResearchConfig m_Config;

    /// <summary>
    /// UI
    /// </summary>
    public GameObject _containerBeforePreUnlock;
    public GameObject _containerAfterPreUnlock;
    public GameObject _containerUnlocked;

    // before pre-unock
    public Text _txtUnlockTotalTime;
    public Text _txtCoinPrice;
    public Image _imgCoinIcon;

    // after pre-unlock
    public Text _txtLeftTime;
    public Text _txtDiamondPrice;
    public Text _txtAdsTimeReduce;
    

    // end UI
    System.DateTime m_dataStartTime;
    System.DateTime m_dateAdsStartTime;
    int m_nLeftTime = 0;

    int m_nAdsLeftTime = 0;

    ResearchManager.eResearchCounterStatus m_eStatus = ResearchManager.eResearchCounterStatus.locked; // 初始状态都是“锁定”

    int m_nLevel = 0;

    List<int> m_lstToActivate = new List<int>();

    District m_BoundTrack = null;

    int m_nRealDiamondCost = 0; // 钻石的价格是随着倒计时实时变化的

    float m_nRealUnlockTime = 0;
    double m_nRealCoinCost = 0;

    UIResearchCounterContainer m_goBoundContainer = null;

    public void Reset()
    {
        SetStatus(ResearchManager.eResearchCounterStatus.locked);
        m_lstToActivate.Clear();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Counting();
        AdsIntervalLoop();
    }

    public void SetConfig(ResearchManager.sResearchConfig config)
    {
        m_Config = config;
    }

    public void SetLevel( int nLevel )
    {
        m_nLevel = nLevel;
    }

    public int GetLevel()
    {
        return m_nLevel;
    }

    public void SetBoundTrack( District track )
    {
        m_BoundTrack = track;
    }

    public District GetBoundTrack()
    {
        return m_BoundTrack;
    }

    void CalculateRealCoinCost()
    {
        m_nRealCoinCost = m_Config.nCoinCost;
        float fCoinCostReducePercent = ScienceTree.s_Instance.GetResearchCointCostReduce();
        if (fCoinCostReducePercent > 0)
        {
            m_nRealCoinCost *= (1f - fCoinCostReducePercent);
        }

        _txtCoinPrice.text = CyberTreeMath.GetFormatMoney(m_nRealCoinCost);
    }

    void CalculateRealUnlockTime()
    {
        m_nRealUnlockTime = m_Config.nTimeCost;
        float fTimeReducePercent = ScienceTree.s_Instance.GetResearchTimeReduce();
        if (fTimeReducePercent > 0)
        {
            m_nRealUnlockTime *= (1f - fTimeReducePercent);
        }
     //   Debug.Log(fTimeReducePercent + "_" + m_Config.nTimeCost + "_" +m_nRealUnlockTime);
        _txtUnlockTotalTime.text = CyberTreeMath.FormatTime((int)m_nRealUnlockTime);
    }

    public void UpdateRealData()
    {
        if (GetStatus() != ResearchManager.eResearchCounterStatus.locked){
            return;
        }

        CalculateRealUnlockTime();
        CalculateRealCoinCost();
    }

    int m_nPlanetId = 0;
    int m_nTrackId = 0;
    public void Activate( int nPlanetId, int nTrackId/*int nLevel, ResearchManager.sResearchConfig config*/ )
    {
        m_nPlanetId = nPlanetId;
        m_nTrackId = nTrackId;
        /*
        if (m_lstToActivate.Count > 0 )
        {
            m_lstToActivate.Add(nLevel);
            return;
        }
        m_lstToActivate.Add(nLevel);
        */

        DoActivate( /*nLevel, config*/);
        /*
        m_Config = config;
        SetLevel( nLevel );
        SetStatus(ResearchManager.eResearchCounterStatus.locked);
        _containerBeforePreUnlock.gameObject.SetActive( true );
        _containerAfterPreUnlock.gameObject.SetActive(false);
        _containerUnlocked.gameObject.SetActive(false);

        _txtUnlockTotalTime.text = CyberTreeMath.FormatTime( m_Config.nTimeCost );
        _txtCoinPrice.text = CyberTreeMath.FormatMoney(m_Config.nCoinCost);
        _imgCoinIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId( MapManager.s_Instance.GetCurPlanet().GetId() );

        */
    }



    public void DoActivate(/*int nLevel, ResearchManager.sResearchConfig config*/)
    {
      // m_Config = config;
      //  SetLevel(nLevel);   
        SetStatus(ResearchManager.eResearchCounterStatus.locked);
        _containerBeforePreUnlock.gameObject.SetActive(true);
        _containerAfterPreUnlock.gameObject.SetActive(false);
        _containerUnlocked.gameObject.SetActive(false);

        UpdateRealData();

        CalculateRealUnlockTime();
        /*
        // 解锁所需要的时间，会受天赋系统的影响
        float nRealTime = m_Config.nTimeCost;
        float fTimeReducePercent = ScienceTree.s_Instance.GetResearchTimeReduce();
        if (fTimeReducePercent > 0)
        {
            nRealTime *= (1f - fTimeReducePercent);
        }

        _txtUnlockTotalTime.text = CyberTreeMath.FormatTime((int)nRealTime);
        */

        // 解锁所需要的金币，会受天赋系统的影响
        //double nRealCost = m_Config.nCoinCost;
        CalculateRealCoinCost();


       // _txtCoinPrice.text = CyberTreeMath.FormatMoney(nRealCost);
        _imgCoinIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nPlanetId);

       
        this.gameObject.SetActive( true );

        EndAdsInterval();
        
    }

    public void BeginAdsInterval( bool bSetStartTimeMannual = false  )
    {
        if (!bSetStartTimeMannual)
        {
            m_nAdsLeftTime = ResearchManager.s_Instance.GetAdsInterval();
            m_dateAdsStartTime = Main.GetSystemTime();
        }
    }

    void EndAdsInterval()
    {
        m_nAdsLeftTime = 0;
        _txtAdsTimeReduce.text ="-" +  CyberTreeMath.FormatTime( ResearchManager.s_Instance.GetAdsReduceTime() );
    }

    public void SetStatus( ResearchManager.eResearchCounterStatus eStatus )
    {
        m_eStatus = eStatus;

    }

    public ResearchManager.eResearchCounterStatus GetStatus()
    {
        return m_eStatus;
    }

    public void OnClickCouter()
    {
        if ( GetStatus() == ResearchManager.eResearchCounterStatus.locked)
        {
            DoPreUnlock();
        }
    }

    void DoPreUnlock()
    {
        double nCurCoin = AccountSystem.s_Instance.GetCoin();
        if ( nCurCoin < m_Config.nCoinCost )
        {
            UIMsgBox.s_Instance.ShowMsg( "金币不够" );
            return;
        }

        AccountSystem.s_Instance.ChangeCoin(-1, m_Config.nCoinCost);
        UIMsgBox.s_Instance.ShowMsg("能源研究开始！");

        PreUnlockSucceed();

        // 存档
        m_BoundTrack.SaveEnergyResearchData();
    }

    public int GetLeftTime()
    {
        return m_nLeftTime;
    }

    public void SetLeftTime( int nLeftTime )
    {
        m_nLeftTime = nLeftTime;
    }

    public int GetAdsLeftTime()
    {
        return m_nAdsLeftTime;
    }

    public void SetAdsLeftTime(int nAdsLeftTime)
    {
        m_nAdsLeftTime = nAdsLeftTime;
    }

    public void PreUnlockSucceed( bool bMannualSetStartTime = false )
    {
        SetStatus(ResearchManager.eResearchCounterStatus.unlocking);
        _containerAfterPreUnlock.SetActive(true);
        _containerBeforePreUnlock.SetActive(false);


        if (!bMannualSetStartTime)
        {

            m_dataStartTime = Main.GetSystemTime();
            m_nLeftTime = (int)m_nRealUnlockTime;//m_Config.nTimeCost;
        }
       

        m_nRealDiamondCost = m_Config.nDiamondCost;
        _txtDiamondPrice.text = m_nRealDiamondCost.ToString();
    }

    float m_fAdsTimeElapse = 0;
    void AdsIntervalLoop()
    {
        if ( m_nAdsLeftTime <= 0 )
        {
            return;
        }

        m_fAdsTimeElapse += Time.deltaTime;
        if (m_fAdsTimeElapse < 1f)
        {
            return;
        }
        m_fAdsTimeElapse = 0;

        int nTimeElapse = (int)((Main.GetSystemTime() - m_dateAdsStartTime).TotalSeconds);
        int nTimeLeft =  m_nAdsLeftTime -  nTimeElapse;
        _txtAdsTimeReduce.text = CyberTreeMath.FormatTime(nTimeLeft);
        if (nTimeLeft <= 0)
        {
            m_nAdsLeftTime = 0;
            EndAdsInterval();
        }

       

    }

    // 广告正在冷却
    bool IsAdsColddowning()
    {
        return m_nAdsLeftTime > 0;
    }

    // 看广告减少解锁等待时间
    public void OnClickButton_WatchAdsToReduceTime()
    {
        if (IsAdsColddowning())
        {
            UIMsgBox.s_Instance.ShowMsg( "广告冷却中" );
            return;
        }

        AdsManager.s_Instance.PlayAds();
        m_nLeftTime -= ResearchManager.s_Instance.GetAdsReduceTime();

        BeginAdsInterval();

        // 存档
        m_BoundTrack.SaveEnergyResearchData();
    }

    // 用钻石直接解锁
    public void OnClickButton_UseDiamondToUnlock()
    {
        double nCurDiamond = AccountSystem.s_Instance.GetGreenCash();
        if (nCurDiamond < m_nRealDiamondCost/*m_Config.nDiamondCost*/ )
        {
            UIMsgBox.s_Instance.ShowMsg("钻石不够!");
            return;
        }
        AccountSystem.s_Instance.SetGreenCash(AccountSystem.s_Instance.GetGreenCash() - m_nRealDiamondCost/*m_Config.nDiamondCost*/);
        UnlockSucceeded();
    }

    public void SetActive( bool bActive )
    {
        this.gameObject.SetActive(bActive);
        if (m_goBoundContainer)
        {
            m_goBoundContainer.gameObject.SetActive(bActive);
        }
    }

    public void UnlockSucceeded( bool bLoad = false )
    {

        SetStatus(ResearchManager.eResearchCounterStatus.unlocked);
        _containerAfterPreUnlock.SetActive(false);
        // this.gameObject.SetActive(false);
        // m_goBoundContainer.SetActive(false);
        SetActive( false );
        if (bLoad)
        {
            return;
        }



        UIMsgBox.s_Instance.ShowMsg("解锁成功!");

        //_containerUnlocked.SetActive( true );
      




        /*
        m_lstToActivate.Remove(GetLevel());
        if (m_lstToActivate.Count > 0) // 如果有积压的待解锁的“瓶颈等级”
        {
            bool bFound = false;
            int nLevel = m_lstToActivate[0];
            ResearchManager.sResearchConfig config = ResearchManager.s_Instance.GetResearchConfig( GetBoundTrack().GetBoundPlanet().GetId(),
                                                                                                   GetBoundTrack().GetId(),
                                                                                                   nLevel, 
                                                                                                  ref bFound);
            DoActivate(nLevel, config);
        }
        */
       

   
        if (m_BoundTrack == MapManager.s_Instance.GetCurDistrict())
        {
            m_BoundTrack.CalculateDropInfo();
            TanGeChe.s_Instance.UpdateCarMallInfo();
        }
        // 存档
        m_BoundTrack.SaveEnergyResearchData();

    }

    float m_fTimeElaspe = 0f;

    public void Counting()
    {
        if ( GetStatus() != ResearchManager.eResearchCounterStatus.unlocking )
        {
            return;
        }

        m_fTimeElaspe += Time.deltaTime;
        if (m_fTimeElaspe < 1f)
        {
            return;
        }
        m_fTimeElaspe = 0;

        int fTimeSpan = (int)((Main.GetSystemTime() - m_dataStartTime).TotalSeconds);
        int nTimeLeft = (int)(m_nLeftTime - fTimeSpan);
        if (nTimeLeft<= 0)
        {
            UnlockSucceeded();
        }
        _txtLeftTime.text = CyberTreeMath.FormatTime(nTimeLeft);

        // 根据时间的推移，更新钻石购买价格
        float fPercent = (float)nTimeLeft / (float)m_Config.nTimeCost;
        if (fPercent <= 0.95f)
        {
            m_nRealDiamondCost = (int)(fPercent * m_Config.nDiamondCost);
            _txtDiamondPrice.text = m_nRealDiamondCost.ToString();
        }

    }

    public System.DateTime GetStartTime()
    {
        return m_dataStartTime;
    }

    public void SetStartTime(System.DateTime dtStartTime)
    {
        m_dataStartTime = dtStartTime;
    }

    public System.DateTime GetAdsStartTime()
    {
        return m_dateAdsStartTime;
    }

    public void SetAdsStartTime(System.DateTime dtAdsStartTime)
    {
        m_dateAdsStartTime = dtAdsStartTime;
    }

    public void BindContainer( UIResearchCounterContainer goContainer)
    {
        m_goBoundContainer = goContainer;
        this.transform.SetParent(m_goBoundContainer.transform);
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
        vecTempPos.x = 0f;
        vecTempPos.y = 0f;
        vecTempPos.z = 0f;
        this.transform.localPosition = vecTempPos;

        goContainer.GetComponent<UIResearchCounterContainer>().SetContainerVisible( false );

        m_goBoundContainer.BindResearchCounter( this );
    }

} // end class
