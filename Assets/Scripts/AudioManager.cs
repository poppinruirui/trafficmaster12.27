﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public static AudioManager s_Instance = null;

    public AudioSource[] m_arySE;

    public GameObject[] m_aryAuidoPrefabs;

    public AudioSource _BMG;

    private void Awake()
    {
        s_Instance = this;
    }

    public enum eSE
    {
        e_trigger_money,
        e_buy_plane,
        e_open_box,
        e_merge_succeed,
        e_begin_run_on_airline,
        e_bank,
        e_small_coin,
        e_congratulations,
        e_firework,
        e_greate_merge,
    };


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlaySE(eSE id )
    {
        /*
        if ( m_arySE[(int)id].isPlaying )
        {
            return;
        }

        m_arySE[(int)id].Play();
        */

        AudioSource audio = NewSE(id);
        audio.Play();

    }

    public AudioSource NewSE(eSE id)
    {
        List<AudioSource> lst = null;
        if ( !m_dicPlayingSE.TryGetValue(id, out lst ) )
        {
            lst = new List<AudioSource>();
            m_dicPlayingSE[id] = lst;
        }
        for (int i = 0; i < lst.Count; i++ )
        {
            if ( !lst[i].isPlaying )
            {
                return lst[i];
            }
        }

        AudioSource audioSource = GameObject.Instantiate(m_aryAuidoPrefabs[(int)id]).GetComponent<AudioSource>();
        lst.Add(audioSource);
        return audioSource;
    }

    Dictionary<eSE, List<AudioSource>> m_dicPlayingSE = new Dictionary<eSE, List<AudioSource>>();

    bool m_bBGM = true;
    public void ToggleBMG()
    {
        m_bBGM = !m_bBGM;
        if (m_bBGM)
        {
            _BMG.Play();
        }
        else
        {
            _BMG.Stop();
        }
    }

}
