﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAdventureCounter : MonoBehaviour {

    public int m_nDuration = 0;

    /// <summary>
    /// UI
    /// </summary>
    


    /// <summary>
    /// Logic Data
    /// </summary>

    public UIAdventureProfitCounter[] m_aryProfitList;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClick_Go()
    {
        AdventureManager.s_Instance.BeginAdventure(this);
    }

} // end class
