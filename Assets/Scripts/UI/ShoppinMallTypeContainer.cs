﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShoppinMallTypeContainer : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public GameObject m_goItemsContainer;

    Vector2 m_vecStartPos = new Vector2();

    int m_nCurNum = 0;
    int m_nRow = 0;

    List<UIShoppinAndItemCounter> m_lstItems = new List<UIShoppinAndItemCounter>();

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtTypeName;

    //// end UI

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Reset()
    {
        ClearAllItems();
        m_nCurNum = 0;
        m_nRow = 0;
    }

    public void AddItem( UIShoppinAndItemCounter item )
    {
//        Debug.Log( "add item: " + item.GetItemType() );
        item.transform.SetParent(m_goItemsContainer.transform);
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        item.transform.localScale = vecTempScale;

        int nCol = m_nCurNum % ShoppinMall.s_Instance.m_nStandardCounterNumPerRow;
        m_nRow = m_nCurNum / ShoppinMall.s_Instance.m_nStandardCounterNumPerRow;
        vecTempPos.x = ShoppinMall.s_Instance.m_fStandardCounterWidth * nCol;
        vecTempPos.y = -ShoppinMall.s_Instance.GetCounterHeight( item.GetItemType() )/*ShoppinMall.s_Instance.m_fStandardCounterHeight*/ * m_nRow;
        vecTempPos.z = 0f;
        item.transform.localPosition = vecTempPos;

        m_lstItems.Add(item);

        m_nCurNum++;
    }

    public int GetRow()
    {
        return (m_nRow + 1);
    }

    public void ClearAllItems()
    {
        /*
        foreach( Transform child in m_goItemsContainer.transform) // 要删除节点的操作绝不能用这种foreach
        {
            UIShoppinAndItemCounter item = child.gameObject.GetComponent<UIShoppinAndItemCounter>();
            ShoppinMall.s_Instance.DeleteCounter(item);
        }
        */
        for (int i = 0; i < m_lstItems.Count; i++ )
        {
            ShoppinMall.s_Instance.DeleteCounter(m_lstItems[i]);
        }
        m_lstItems.Clear();
    }

    public void RefreshPrice()
    {
        for (int i = 0; i < m_lstItems.Count; i++)
        {
            m_lstItems[i].Init(m_lstItems[i].m_Config);
        }
    }

    public float CalculateHeight( ShoppinMall.eItemType eItemType )
    {
        float fHeight = 0f;

        fHeight += ShoppinMall.s_Instance.m_fTypeContainerTitleHeight;
        fHeight += ( GetRow() * ShoppinMall.s_Instance.GetCounterHeight(eItemType) );


        return fHeight;
    }

} // end class
