﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour {

    public static UIManager s_Instance = null;

    // 需要适配的UI



    public Canvas _canvasMain;
    public GameObject _UI;
    public CanvasScaler _canvasScaler;

    public Vector2 m_vecCoinFlySeesion0EndPosRangeX = new Vector2();
    public Vector2 m_vecCoinFlySeesion0EndPosRangeY = new Vector2();
    public Vector2 m_vecCoinFlyStartPos = new Vector2();
    public Vector2 m_vecCoinFlyEndPos = new Vector2();
    public float m_fSession0Time = 0.5f;
    public float m_fSession1Time = 1.5f;

    public GameObject _containerFlyingCoins;

    /// <summary>
    /// UI
    /// </summary>
    public GameObject[] m_arySomeCtrlsDueToXingKong;

    // end UI


    public CFrameAnimationEffect _goEffectClick;

    public enum eUiId
    {
        homepage_prestige_btn,
        homepage_container_money,
        homepage_gain_overview,
        homepage_dps_container,
        homepage_skills_panel,
        homepage_bottom_buttons,
        homepage_cheat_btn,

        shoppinmall_top_money_counters,

        bigmap_top_money_counters,


    };

    public Vector2[] m_aryUICtrlPos_1920_1080;
    public Vector2[] m_aryUICtrlPos_2436_1125;
    public GameObject[] m_aryUICtrl;
    public Vector2[] m_aryResolution;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        /*

        ///// 做适配
        Vector2[] aryPos = null;
        switch (ResolutionManager.s_Instance.m_eResolution)
        {
            case ResolutionManager.eResolution.e_1920_1080:
                {
                    aryPos = m_aryUICtrlPos_1920_1080;
                    _canvasScaler.referenceResolution = m_aryResolution[(int)ResolutionManager.eResolution.e_1920_1080];
                }
                break;
            case ResolutionManager.eResolution.e_2436_1125:
                {
                    aryPos = m_aryUICtrlPos_2436_1125;
                    _canvasScaler.referenceResolution = m_aryResolution[(int)ResolutionManager.eResolution.e_2436_1125];
                }
                break;
        } // end switch


        for (int i = 0; i < m_aryUICtrl.Length; i++ )
        {
            GameObject ctrl = m_aryUICtrl[i];
            if ( ctrl == null )
            {
                continue;
            }

            ctrl.transform.localPosition = aryPos[i];

        } // end for i

        */
        ///// end 做适配



        Text[] aryTexts =  this.GetComponentsInChildren<Text>();
        for (int i = 0; i < aryTexts.Length; i++ )
        {
            Text txt = aryTexts[i];
            txt.font = ResourceManager.s_Instance._font;
        }
	}
	
	// Update is called once per frame
	void Update () {
        OnTapTap();

    }

    public static void UpdateDropdownView(Dropdown dropdownItem, List<string> showNames)
    {
        dropdownItem.options.Clear();
        Dropdown.OptionData tempData;
        for (int i = 0; i < showNames.Count; i++)
        {
            tempData = new Dropdown.OptionData();
            tempData.text = showNames[i];
            dropdownItem.options.Add(tempData);
        }
        dropdownItem.captionText.text = showNames[0];
    }

    public static bool IsPointerOverUI()
    {
        PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
        eventData.pressPosition = Input.mousePosition;
        eventData.position = Input.mousePosition;

        List<RaycastResult> list = new List<RaycastResult>();
        UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);

        return list.Count > 0;

    }

    public void SetSomeUiVisibleDueToStarSky( bool bVisible )
    {
        for (int i = 0; i < m_arySomeCtrlsDueToXingKong.Length; i++ )
        {
            if ( m_arySomeCtrlsDueToXingKong[i] != null )
            {
                m_arySomeCtrlsDueToXingKong[i].SetActive( bVisible );
            }
        }
    }

    
    public void OnTapTap()
    {
        if ( Input.GetMouseButtonDown(0) )
        {
         //   ShowClickEffect();
        }

        if (Input.GetMouseButtonUp(0))
        {
         //   ShowClickEffect();
        }

    }

    public void ShowClickEffect()
    {
        Vector2 _pos = Vector2.one;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvasMain.transform as RectTransform,
                Input.mousePosition, _canvasMain.worldCamera, out _pos);
        _goEffectClick.transform.localPosition = _pos;
        _goEffectClick.gameObject.SetActive( true );
        _goEffectClick.BeginPlay(false);

    }

    public void SetUiVisible( bool bVisible )
    {
        _UI.SetActive( bVisible );
    }

} // end class
