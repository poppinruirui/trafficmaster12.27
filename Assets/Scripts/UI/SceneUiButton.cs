﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneUiButton : MonoBehaviour {

    public delegate void OnClickEventHandler();

    public event OnClickEventHandler OnClickEvent;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnMouseUp()
    {
        if (OnClickEvent != null)
        {
            OnClickEvent();
        }
    }

  

}
