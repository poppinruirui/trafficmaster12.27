﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseRotate : MonoBehaviour {

    public bool m_bRotating;
    public float m_fRotateSpeed;

    float m_fCurAngle = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FixedUpdate()
    {
        if ( !m_bRotating )
        {
            return;
        }

        m_fCurAngle += m_fRotateSpeed * Time.fixedDeltaTime;
        TransformManager.RotateObj(this.gameObject, m_fCurAngle);
    }

} // end class
