﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : MonoBehaviour {

    public static EffectManager s_Instance = null;

    public enum eEffectType
    {
        merge_succeed, // 合成成功
        recylce,       // 销毁
        open_box,       // 开宝箱
        unlock_new_plane_explosion,

        firework0,
        firework1,
        firework2,
        firework3,
        firework4,
        firework5,

        accelerate_light,
    };

    public GameObject[] m_aryEffectPrefabs;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject NewEffect( eEffectType eType )
    {
        return GameObject.Instantiate( m_aryEffectPrefabs[(int)eType] );
    }

    public void DeleteEffect( GameObject goEffect )
    {
        GameObject.Destroy( goEffect );
    }


}
