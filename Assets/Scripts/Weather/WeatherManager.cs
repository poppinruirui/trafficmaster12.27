﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherManager : MonoBehaviour {

    public GameObject m_preSnowFlake; // “雪花”元件

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public SnowFlake NewSnowFlake()
    {
        SnowFlake snow = GameObject.Instantiate( m_preSnowFlake ).GetComponent<SnowFlake>();
        return snow;
    }

    public void DeleteSnowFlake(SnowFlake snow)
    {
        GameObject.Destroy( snow.gameObject );
    }


} // end class
