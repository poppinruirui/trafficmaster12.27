﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowFlake : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();
    static Color colorTemp = new Color();// 

    public SpriteRenderer _srMain;

    float m_fDropEndPosY = 0;

    float m_fAlphaChangePerFrame = 0f;
    float m_fBeginFadePosY = 0f;

    float m_fCurAngle = 0f;

    float m_fStartPosX = 0f;
    float m_fStartPosY = 0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetAlpha( float fAlpha )
    {
        colorTemp = _srMain.color;
        colorTemp.a = fAlpha;
        _srMain.color = colorTemp;
    }

    public void SetPos( Vector3 pos )
    {
        this.transform.localPosition = pos;
    }

    public Vector3 GetPos()
    {
        return this.transform.localPosition;
    }

    void Fade()
    {
        if ( GetPos().y > m_fBeginFadePosY )
        {
            return;
        }

        colorTemp = this._srMain.color;
        colorTemp.a += m_fAlphaChangePerFrame;
        this._srMain.color = colorTemp;
    }

    public bool Drop( float fSpeed, float fEndPos )
    {
        Fade();

        m_fCurAngle += 0.25f;

        vecTempPos = this.GetPos();
        vecTempPos.y += fSpeed;
        vecTempPos.x = m_fStartPosX + Mathf.Sin( CyberTreeMath.Angle2Radian(m_fCurAngle) ) * 0.3f;
        if (vecTempPos.y <= m_fDropEndPosY)
        {
            return true;
        }
        SetPos( vecTempPos );

        return false;
    }

    public void SetDropEndPosY( float fDropEndPosY)
    {
        m_fDropEndPosY = fDropEndPosY;
    }

    public void SetScale( float fScale )
    {
        vecTempScale.x = fScale;
        vecTempScale.y = fScale;
        vecTempScale.z = 1f;
        this.transform.localScale = vecTempScale;
    }

    public void SetFadeParams( float fAlphaChangePerFrame, float fBeginFadePosY)
    {
        m_fAlphaChangePerFrame = fAlphaChangePerFrame;
        m_fBeginFadePosY = fBeginFadePosY;
    }

    public void SetStartPos( float fStartPosX, float fStartPosY)

    {
        m_fStartPosX = fStartPosX;
        m_fStartPosY = fStartPosY;
    }

} // end class
