﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiaoZiJinBi : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();

    public float m_fT = 0f;
    public float m_fSX = 0f;
    public float m_fSY = 0f;

    float m_fV0X = 0f;
    float m_fV0Y = 0f;
    float m_fA = 0f;

    int m_nStatus = 0;

    float m_fDir = 1f;

	// Use this for initialization
	void Start () {
		
	}

    private void FixedUpdate()
    {
        Loop();
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void Begin( float SX = 6, float SY = 6, float T = 0.2f )
    {
       
        m_fT = T; 

        float fTemp = UnityEngine.Random.Range(0f, 10f);
        if (fTemp <= 2.5 || ( fTemp > 5 && fTemp < 7.5 ) )
        {
            m_fDir = 1;
        }
        else
        {
            m_fDir = -1;
        }
      

        fTemp = UnityEngine.Random.Range(0.1f, 2f);
        m_fSX = SX * fTemp;
        fTemp = UnityEngine.Random.Range(0.8f, 1.5f);
        m_fSY = SY * fTemp;

        m_fA = CyberTreeMath.GetA( m_fSY, m_fT );
        m_fV0Y = CyberTreeMath.GetV0(m_fSY, m_fT);
        m_fV0X = m_fSX / (2 * m_fT) * m_fDir;

        vecTempPos.x = 0f;
        vecTempPos.y = 0f;
        vecTempPos.z= -100f;
        SetPos( vecTempPos );

        m_nStatus = 1;
    }

    void Loop()
    {
        if (m_nStatus == 1)
        {
            vecTempPos = GetPos();
            vecTempPos.x += m_fV0X * Time.fixedDeltaTime;
            vecTempPos.y += m_fV0Y * Time.fixedDeltaTime;
            SetPos( vecTempPos );
            m_fV0Y += m_fA * Time.fixedDeltaTime;
            if (m_fV0Y <= 0f)
            {
                m_nStatus = 2;
            }
        }
        else if (m_nStatus == 2)
        {
            vecTempPos = GetPos();
            vecTempPos.x += m_fV0X * Time.fixedDeltaTime;
            vecTempPos.y += m_fV0Y * Time.fixedDeltaTime;
            SetPos(vecTempPos);
            m_fV0Y += m_fA * Time.fixedDeltaTime;
            if (vecTempPos.y <= 0f)
            {
                End();
            }
        }

    }

    void End()
    {
        m_nStatus = 0;

        ResourceManager.s_Instance.DeleteJinBi( this.gameObject );
    }

    public void SetPos( Vector3 pos )
    {
        this.transform.localPosition = pos;
    }

    public Vector3 GetPos()
    {
        return this.transform.localPosition;
    }


}
