using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
IntenseVibration is a straight-forward wrapper for the 
android Vibrator class with the android Vibrator API exposed 
as directly as possible. It also includes auxiliary 
functionality for variable intensity vibrations.

The class is a singleton - access it with 
IntenseVibration.Instance. The first time you access 
Instance will initialise the singleton and create a gameobject 
(IntenseVibrationObject) which you don't need to do anything 
with. You may notice a slight lag on the initialisation - if 
you want to avoid this during gameplay, simply access the
instance at startup somewhere.

The comments should cover everything you need to know to use 
this class.

The class shouldn't fail on platforms other than android.
However, no attempt has been made to have the functionality
reproduced on iPhone as it is simply not possible for 
advanced vibration on the iPhone.

To use vibrator functionality, the app must specify the 
VIBRATE permission in its manifest file or the app will 
crash with the "force close" message. One of these is 
provided with this code but should be treated only as an 
example and is probably not useful for your specific needs. 
The part we're concerned with is: 
"<uses-permission android:name="android.permission.VIBRATE"/>".

Some function comments are taken from or are modified 
versions of text from Google's API docs which is licensed 
under Apache 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
*/

public class IntenseVibration : MonoBehaviour 
{
	static IntenseVibration mInstance;
    public static IntenseVibration Instance
    {
        get
        {
			if (mInstance == null)
			{
				mInstance = (new GameObject("IntenseVibrationObject")).AddComponent<IntenseVibration>();
			}
			
            return mInstance;
        }
    }
	
	static AndroidJavaObject mJVibrator = null;
	
	/*
	Retrieves the Java Vibrator object
	*/
	void Init()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		using(AndroidJavaClass jUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) 
		{
			using(AndroidJavaObject jCurrentActivity = jUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity")) 
			{
				mJVibrator = jCurrentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
			}
		}
#endif
	}
	
	/*
	Check whether the hardware has a vibrator. 
	
	THIS CALL WILL ONLY WORK WITH API LEVEL >= 11.
	
	Returns
	    True if the hardware has a vibrator, else false. 
	    Also false if API level < 11 as the Java method doesn't exist
	*/
	public bool HasVibrator()
	{
#if UNITY_ANDROID		
		return mJVibrator.Call<bool>("hasVibrator");
#else
		return false;
#endif
	}
	
	/*
	Vibrate constantly for the specified period of time.

	This method requires the caller to hold the permission VIBRATE.
	
	Parameters
		milliseconds 	The number of milliseconds to vibrate. 
	*/
	public void Vibrate(long milliseconds)
	{
#if UNITY_ANDROID
		Cancel();

        if (mJVibrator != null)
        {
            mJVibrator.Call("vibrate", milliseconds);
        }
#endif
	}
	
	/*
	Vibrate with a given pattern.
	
	Pass in an array of ints that are the durations for which to 
	turn on or off the vibrator in milliseconds. The first value 
	indicates the number of milliseconds to wait before turning 
	the vibrator on. The next value indicates the number of 
	milliseconds for which to keep the vibrator on before turning 
	it off. Subsequent values alternate between durations in 
	milliseconds to turn the vibrator off or to turn the vibrator 
	on.
	
	To cause the pattern to repeat, pass the index into the 
	pattern array at which to start the repeat, or -1 to disable 
	repeating.
	
	This method requires the caller to hold the permission VIBRATE.
	
	Parameters
		pattern 	an array of longs of times for which to turn the vibrator on or off.
		repeat 		the index into pattern at which to repeat, or -1 if you don't want to repeat. 
						So use repeat = 0 to repeat indefinitely.
	*/
	public void Vibrate(long[] pattern, int repeat)
	{
#if UNITY_ANDROID
		Cancel();
		
		mJVibrator.Call("vibrate", pattern, repeat);
#endif
	}
	
	/*
	Vibrate constantly for the specified period of time at the 
	specified intensity. Note that the time to vibrate will not 
	match the constant intensity vibrate function exactly as its 
	timing is done by android, not this class. But it tends to be 
	close enough to not be a problem.

	This method requires the caller to hold the permission VIBRATE.
	
	Parameters
		milliseconds 	The number of milliseconds to vibrate. 
		intensity		The intensity of the vibration (0..9)
	*/	
	public void Vibrate(long milliseconds, int intensity)
	{
		if (intensity >= 0 && intensity < 10)
		{
			Vibrate(mPatterns[intensity], 0);
			mVibrateTimeLeft = milliseconds / 1000.0f;
			return;
		}
	}
	
	/*
	Turn the vibrator off.
	
	This method requires the caller to hold the permission VIBRATE. 
	*/
	public void Cancel()
	{
#if UNITY_ANDROID
		mVibrateTimeLeft = 0.0f;

        if (mJVibrator != null)
        {
            mJVibrator.Call("cancel");
        }
#endif
	}
	
	float mVibrateTimeLeft = 0.0f;
	
	List<long[]> mPatterns = new List<long[]>();
	
	void Start()
	{
		Init();
		
		for (int i = 1; i <= 10; ++i)
		{
			mPatterns.Add(new long[2]{10 - i, i * 5});
		}
	}
	
	void Update()
	{		
		if (mVibrateTimeLeft < 0.0f)
		{
			Cancel();
		}
		
		if (mVibrateTimeLeft > 0.0f)
		{
			mVibrateTimeLeft -= Time.deltaTime;
		}
	}
}
